#!/bin/bash

# Function to process file contents
process_file_contents() {
  local file="$1"
  sed -i 's//_/g' "$file"
}

# Function to rename files and process their contents
rename_and_process_files() {
  local dir="$1"
  local pattern="$2"
  local replacement="$3"

  find "$dir" -type f -name "*$pattern*" | while read -r file; do
    new_file=$(echo "$file" | sed "s/$pattern/$replacement/g")
    mv "$file" "$new_file"
    process_file_contents "$new_file"
  done
}

# Rename substrings in filenames and process file contents
rename_and_process_files . "" "_"
rename_and_process_files . "Controllers" ""
rename_and_process_files . "ObjectPeriod" ""