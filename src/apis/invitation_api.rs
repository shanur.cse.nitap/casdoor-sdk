

use reqwest;

use crate::{apis::ResponseContent, models};
use super::{Error, configuration};

/// struct for passing parameters to the method [`add_invitation`]
#[derive(Clone, Debug)]
pub struct ApiControllerAddInvitationParams {
    /// The details of the invitation
    pub body: models::Invitation
}

/// struct for passing parameters to the method [`delete_invitation`]
#[derive(Clone, Debug)]
pub struct ApiControllerDeleteInvitationParams {
    /// The details of the invitation
    pub body: models::Invitation
}

/// struct for passing parameters to the method [`get_invitation`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetInvitationParams {
    /// The id ( owner/name ) of the invitation
    pub id: String
}

/// struct for passing parameters to the method [`get_invitations`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetInvitationsParams {
    /// The owner of invitations
    pub owner: String
}

/// struct for passing parameters to the method [`update_invitation`]
#[derive(Clone, Debug)]
pub struct ApiControllerUpdateInvitationParams {
    /// The id ( owner/name ) of the invitation
    pub id: String,
    /// The details of the invitation
    pub body: models::Invitation
}

/// struct for passing parameters to the method [`verify_invitation`]
#[derive(Clone, Debug)]
pub struct ApiControllerVerifyInvitationParams {
    /// The id ( owner/name ) of the invitation
    pub id: String
}


/// struct for typed errors of method [`add_invitation`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerAddInvitationError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`delete_invitation`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerDeleteInvitationError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_invitation`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetInvitationError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_invitations`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetInvitationsError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`update_invitation`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerUpdateInvitationError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`verify_invitation`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerVerifyInvitationError {
    UnknownValue(serde_json::Value),
}


/// add invitation
pub async fn add_invitation(configuration: &configuration::Configuration, params: ApiControllerAddInvitationParams) -> Result<models::ControllersResponse, Error<ApiControllerAddInvitationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/add-invitation", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerAddInvitationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// delete invitation
pub async fn delete_invitation(configuration: &configuration::Configuration, params: ApiControllerDeleteInvitationParams) -> Result<models::ControllersResponse, Error<ApiControllerDeleteInvitationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/delete-invitation", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerDeleteInvitationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get invitation
pub async fn get_invitation(configuration: &configuration::Configuration, params: ApiControllerGetInvitationParams) -> Result<models::Invitation, Error<ApiControllerGetInvitationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-invitation", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetInvitationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get invitations
pub async fn get_invitations(configuration: &configuration::Configuration, params: ApiControllerGetInvitationsParams) -> Result<Vec<models::Invitation>, Error<ApiControllerGetInvitationsError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let owner = params.owner;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-invitations", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("owner", &owner.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetInvitationsError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// update invitation
pub async fn update_invitation(configuration: &configuration::Configuration, params: ApiControllerUpdateInvitationParams) -> Result<models::ControllersResponse, Error<ApiControllerUpdateInvitationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/update-invitation", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerUpdateInvitationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// verify invitation
pub async fn verify_invitation(configuration: &configuration::Configuration, params: ApiControllerVerifyInvitationParams) -> Result<models::ControllersResponse, Error<ApiControllerVerifyInvitationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/verify-invitation", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerVerifyInvitationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

