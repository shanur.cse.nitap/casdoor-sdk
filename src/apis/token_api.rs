

use reqwest;

use crate::{apis::ResponseContent, models};
use super::{Error, configuration};

/// struct for passing parameters to the method [`add_token`]
#[derive(Clone, Debug)]
pub struct ApiControllerAddTokenParams {
    /// Details of the token
    pub body: models::Token
}

/// struct for passing parameters to the method [`delete_token`]
#[derive(Clone, Debug)]
pub struct ApiControllerDeleteTokenParams {
    /// Details of the token
    pub body: models::Token
}

/// struct for passing parameters to the method [`get_captcha_status`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetCaptchaStatusParams {
    /// The id ( owner/name ) of user
    pub id: String
}

/// struct for passing parameters to the method [`get_o_auth_token`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetOAuthTokenParams {
    /// OAuth grant type
    pub grant_type: String,
    /// OAuth client id
    pub client_id: String,
    /// OAuth client secret
    pub client_secret: String,
    /// OAuth code
    pub code: String
}

/// struct for passing parameters to the method [`get_token`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetTokenParams {
    /// The id ( owner/name ) of token
    pub id: String
}

/// struct for passing parameters to the method [`get_tokens`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetTokensParams {
    /// The owner of tokens
    pub owner: String,
    /// The size of each page
    pub page_size: String,
    /// The number of the page
    pub p: String
}

/// struct for passing parameters to the method [`refresh_token`]
#[derive(Clone, Debug)]
pub struct ApiControllerRefreshTokenParams {
    /// OAuth grant type
    pub grant_type: String,
    /// OAuth refresh token
    pub refresh_token: String,
    /// OAuth scope
    pub scope: String,
    /// OAuth client id
    pub client_id: String,
    /// OAuth client secret
    pub client_secret: Option<String>
}

/// struct for passing parameters to the method [`update_token`]
#[derive(Clone, Debug)]
pub struct ApiControllerUpdateTokenParams {
    /// The id ( owner/name ) of token
    pub id: String,
    /// Details of the token
    pub body: models::Token
}


/// struct for typed errors of method [`add_token`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerAddTokenError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`delete_token`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerDeleteTokenError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_captcha_status`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetCaptchaStatusError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_o_auth_token`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetOAuthTokenError {
    Status400(models::TokenError),
    Status401(models::TokenError),
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_token`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetTokenError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_tokens`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetTokensError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`refresh_token`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerRefreshTokenError {
    Status400(models::TokenError),
    Status401(models::TokenError),
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`update_token`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerUpdateTokenError {
    UnknownValue(serde_json::Value),
}


/// add token
pub async fn add_token(configuration: &configuration::Configuration, params: ApiControllerAddTokenParams) -> Result<models::ControllersResponse, Error<ApiControllerAddTokenError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/add-token", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerAddTokenError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// delete token
pub async fn delete_token(configuration: &configuration::Configuration, params: ApiControllerDeleteTokenParams) -> Result<models::ControllersResponse, Error<ApiControllerDeleteTokenError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/delete-token", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerDeleteTokenError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// Get Login Error Counts
pub async fn get_captcha_status(configuration: &configuration::Configuration, params: ApiControllerGetCaptchaStatusParams) -> Result<models::ControllersResponse, Error<ApiControllerGetCaptchaStatusError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-captcha-status", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetCaptchaStatusError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get OAuth access token
pub async fn get_o_auth_token(configuration: &configuration::Configuration, params: ApiControllerGetOAuthTokenParams) -> Result<models::TokenWrapper, Error<ApiControllerGetOAuthTokenError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let grant_type = params.grant_type;
    let client_id = params.client_id;
    let client_secret = params.client_secret;
    let code = params.code;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/login/oauth/access_token", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("grant_type", &grant_type.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("client_id", &client_id.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("client_secret", &client_secret.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("code", &code.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetOAuthTokenError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get token
pub async fn get_token(configuration: &configuration::Configuration, params: ApiControllerGetTokenParams) -> Result<models::Token, Error<ApiControllerGetTokenError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-token", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetTokenError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get tokens
pub async fn get_tokens(configuration: &configuration::Configuration, params: ApiControllerGetTokensParams) -> Result<Vec<models::Token>, Error<ApiControllerGetTokensError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let owner = params.owner;
    let page_size = params.page_size;
    let p = params.p;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-tokens", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("owner", &owner.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("pageSize", &page_size.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("p", &p.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetTokensError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// refresh OAuth access token
pub async fn refresh_token(configuration: &configuration::Configuration, params: ApiControllerRefreshTokenParams) -> Result<models::TokenWrapper, Error<ApiControllerRefreshTokenError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let grant_type = params.grant_type;
    let refresh_token = params.refresh_token;
    let scope = params.scope;
    let client_id = params.client_id;
    let client_secret = params.client_secret;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/login/oauth/refresh_token", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("grant_type", &grant_type.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("refresh_token", &refresh_token.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("scope", &scope.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("client_id", &client_id.to_string())]);
    if let Some(ref local_var_str) = client_secret {
        local_var_req_builder = local_var_req_builder.query(&[("client_secret", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerRefreshTokenError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// update token
pub async fn update_token(configuration: &configuration::Configuration, params: ApiControllerUpdateTokenParams) -> Result<models::ControllersResponse, Error<ApiControllerUpdateTokenError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/update-token", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerUpdateTokenError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

