

use reqwest;

use crate::{apis::ResponseContent, models};
use super::{Error, configuration};

/// struct for passing parameters to the method [`add_session`]
#[derive(Clone, Debug)]
pub struct ApiControllerAddSessionParams {
    /// The id(organization/application/user) of session
    pub id: String,
    /// sessionId to be added
    pub session_id: String
}

/// struct for passing parameters to the method [`delete_session`]
#[derive(Clone, Debug)]
pub struct ApiControllerDeleteSessionParams {
    /// The id(organization/application/user) of session
    pub id: String
}

/// struct for passing parameters to the method [`get_sessions`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetSessionsParams {
    /// The organization name
    pub owner: String
}

/// struct for passing parameters to the method [`get_single_session`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetSingleSessionParams {
    /// The id(organization/application/user) of session
    pub id: String
}

/// struct for passing parameters to the method [`is_session_duplicated`]
#[derive(Clone, Debug)]
pub struct ApiControllerIsSessionDuplicatedParams {
    /// The id(organization/application/user) of session
    pub id: String,
    /// sessionId to be checked
    pub session_id: String
}

/// struct for passing parameters to the method [`update_session`]
#[derive(Clone, Debug)]
pub struct ApiControllerUpdateSessionParams {
    /// The id(organization/application/user) of session
    pub id: String
}


/// struct for typed errors of method [`add_session`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerAddSessionError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`delete_session`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerDeleteSessionError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_sessions`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetSessionsError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_single_session`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetSingleSessionError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`is_session_duplicated`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerIsSessionDuplicatedError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`update_session`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerUpdateSessionError {
    UnknownValue(serde_json::Value),
}


/// Add session for one user in one application. If there are other existing sessions, join the session into the list.
pub async fn add_session(configuration: &configuration::Configuration, params: ApiControllerAddSessionParams) -> Result<Vec<String>, Error<ApiControllerAddSessionError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;
    let session_id = params.session_id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/add-session", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("sessionId", &session_id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerAddSessionError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// Delete session for one user in one application.
pub async fn delete_session(configuration: &configuration::Configuration, params: ApiControllerDeleteSessionParams) -> Result<Vec<String>, Error<ApiControllerDeleteSessionError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/delete-session", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerDeleteSessionError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// Get organization user sessions.
pub async fn get_sessions(configuration: &configuration::Configuration, params: ApiControllerGetSessionsParams) -> Result<Vec<String>, Error<ApiControllerGetSessionsError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let owner = params.owner;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-sessions", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("owner", &owner.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetSessionsError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// Get session for one user in one application.
pub async fn get_single_session(configuration: &configuration::Configuration, params: ApiControllerGetSingleSessionParams) -> Result<Vec<String>, Error<ApiControllerGetSingleSessionError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-session", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetSingleSessionError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// Check if there are other different sessions for one user in one application.
pub async fn is_session_duplicated(configuration: &configuration::Configuration, params: ApiControllerIsSessionDuplicatedParams) -> Result<Vec<String>, Error<ApiControllerIsSessionDuplicatedError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;
    let session_id = params.session_id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/is-session-duplicated", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("sessionId", &session_id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerIsSessionDuplicatedError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// Update session for one user in one application.
pub async fn update_session(configuration: &configuration::Configuration, params: ApiControllerUpdateSessionParams) -> Result<Vec<String>, Error<ApiControllerUpdateSessionError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/update-session", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerUpdateSessionError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

