

use reqwest;

use crate::{apis::ResponseContent, models};
use super::{Error, configuration};

/// struct for passing parameters to the method [`add_resource`]
#[derive(Clone, Debug)]
pub struct ApiControllerAddResourceParams {
    /// Resource object
    pub resource: models::Resource
}

/// struct for passing parameters to the method [`delete_resource`]
#[derive(Clone, Debug)]
pub struct ApiControllerDeleteResourceParams {
    /// Resource object
    pub resource: models::Resource
}

/// struct for passing parameters to the method [`get_resource`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetResourceParams {
    /// The id ( owner/name ) of resource
    pub id: String
}

/// struct for passing parameters to the method [`get_resources`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetResourcesParams {
    /// Owner
    pub owner: String,
    /// User
    pub user: String,
    /// Page Size
    pub page_size: Option<i32>,
    /// Page Number
    pub p: Option<i32>,
    /// Field
    pub field: Option<String>,
    /// Value
    pub value: Option<String>,
    /// Sort Field
    pub sort_field: Option<String>,
    /// Sort Order
    pub sort_order: Option<String>
}

/// struct for passing parameters to the method [`update_resource`]
#[derive(Clone, Debug)]
pub struct ApiControllerUpdateResourceParams {
    /// The id ( owner/name ) of resource
    pub id: String,
    /// The resource object
    pub resource: models::Resource
}

/// struct for passing parameters to the method [`upload_resource`]
#[derive(Clone, Debug)]
pub struct ApiControllerUploadResourceParams {
    /// Owner
    pub owner: String,
    /// User
    pub user: String,
    /// Application
    pub application: String,
    /// Full File Path
    pub full_file_path: String,
    /// Resource file
    pub file: std::path::PathBuf,
    /// Tag
    pub tag: Option<String>,
    /// Parent
    pub parent: Option<String>,
    /// Created Time
    pub created_time: Option<String>,
    /// Description
    pub description: Option<String>
}


/// struct for typed errors of method [`add_resource`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerAddResourceError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`delete_resource`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerDeleteResourceError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_resource`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetResourceError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_resources`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetResourcesError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`update_resource`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerUpdateResourceError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`upload_resource`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerUploadResourceError {
    UnknownValue(serde_json::Value),
}


pub async fn add_resource(configuration: &configuration::Configuration, params: ApiControllerAddResourceParams) -> Result<models::ControllersResponse, Error<ApiControllerAddResourceError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let resource = params.resource;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/add-resource", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&resource);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerAddResourceError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

pub async fn delete_resource(configuration: &configuration::Configuration, params: ApiControllerDeleteResourceParams) -> Result<models::ControllersResponse, Error<ApiControllerDeleteResourceError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let resource = params.resource;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/delete-resource", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&resource);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerDeleteResourceError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get resource
pub async fn get_resource(configuration: &configuration::Configuration, params: ApiControllerGetResourceParams) -> Result<models::Resource, Error<ApiControllerGetResourceError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-resource", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetResourceError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get resources
pub async fn get_resources(configuration: &configuration::Configuration, params: ApiControllerGetResourcesParams) -> Result<Vec<models::Resource>, Error<ApiControllerGetResourcesError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let owner = params.owner;
    let user = params.user;
    let page_size = params.page_size;
    let p = params.p;
    let field = params.field;
    let value = params.value;
    let sort_field = params.sort_field;
    let sort_order = params.sort_order;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-resources", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("owner", &owner.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("user", &user.to_string())]);
    if let Some(ref local_var_str) = page_size {
        local_var_req_builder = local_var_req_builder.query(&[("pageSize", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = p {
        local_var_req_builder = local_var_req_builder.query(&[("p", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = field {
        local_var_req_builder = local_var_req_builder.query(&[("field", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = value {
        local_var_req_builder = local_var_req_builder.query(&[("value", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = sort_field {
        local_var_req_builder = local_var_req_builder.query(&[("sortField", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = sort_order {
        local_var_req_builder = local_var_req_builder.query(&[("sortOrder", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetResourcesError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get resource
pub async fn update_resource(configuration: &configuration::Configuration, params: ApiControllerUpdateResourceParams) -> Result<models::ControllersResponse, Error<ApiControllerUpdateResourceError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;
    let resource = params.resource;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/update-resource", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&resource);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerUpdateResourceError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

pub async fn upload_resource(configuration: &configuration::Configuration, params: ApiControllerUploadResourceParams) -> Result<models::Resource, Error<ApiControllerUploadResourceError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let owner = params.owner;
    let user = params.user;
    let application = params.application;
    let full_file_path = params.full_file_path;
    let file = params.file;
    let tag = params.tag;
    let parent = params.parent;
    let created_time = params.created_time;
    let description = params.description;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/upload-resource", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("owner", &owner.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("user", &user.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("application", &application.to_string())]);
    if let Some(ref local_var_str) = tag {
        local_var_req_builder = local_var_req_builder.query(&[("tag", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = parent {
        local_var_req_builder = local_var_req_builder.query(&[("parent", &local_var_str.to_string())]);
    }
    local_var_req_builder = local_var_req_builder.query(&[("fullFilePath", &full_file_path.to_string())]);
    if let Some(ref local_var_str) = created_time {
        local_var_req_builder = local_var_req_builder.query(&[("createdTime", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = description {
        local_var_req_builder = local_var_req_builder.query(&[("description", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    let mut local_var_form = reqwest::multipart::Form::new();
    // TODO: support file upload for 'file' parameter
    local_var_req_builder = local_var_req_builder.multipart(local_var_form);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerUploadResourceError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

