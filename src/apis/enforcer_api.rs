

use reqwest;

use crate::{apis::ResponseContent, models};
use super::{Error, configuration};

/// struct for passing parameters to the method [`add_enforcer`]
#[derive(Clone, Debug)]
pub struct ApiControllerAddEnforcerParams {
    /// The enforcer object
    pub enforcer: serde_json::Value
}

/// struct for passing parameters to the method [`batch_enforce`]
#[derive(Clone, Debug)]
pub struct ApiControllerBatchEnforceParams {
    /// array of casbin requests
    pub body: Vec<String>,
    /// permission id
    pub permission_id: Option<String>,
    /// model id
    pub model_id: Option<String>,
    /// owner
    pub owner: Option<String>
}

/// struct for passing parameters to the method [`delete_enforcer`]
#[derive(Clone, Debug)]
pub struct ApiControllerDeleteEnforcerParams {
    /// The enforcer object
    pub body: models::Enforcer
}

/// struct for passing parameters to the method [`enforce`]
#[derive(Clone, Debug)]
pub struct ApiControllerEnforceParams {
    /// Casbin request
    pub body: Vec<String>,
    /// permission id
    pub permission_id: Option<String>,
    /// model id
    pub model_id: Option<String>,
    /// resource id
    pub resource_id: Option<String>,
    /// owner
    pub owner: Option<String>
}

/// struct for passing parameters to the method [`get_enforcer`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetEnforcerParams {
    /// The id ( owner/name )  of enforcer
    pub id: String
}

/// struct for passing parameters to the method [`get_enforcers`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetEnforcersParams {
    /// The owner of enforcers
    pub owner: String
}

/// struct for passing parameters to the method [`update_enforcer`]
#[derive(Clone, Debug)]
pub struct ApiControllerUpdateEnforcerParams {
    /// The id ( owner/name )  of enforcer
    pub id: String,
    /// The enforcer object
    pub enforcer: serde_json::Value
}


/// struct for typed errors of method [`add_enforcer`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerAddEnforcerError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`batch_enforce`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerBatchEnforceError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`delete_enforcer`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerDeleteEnforcerError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`enforce`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerEnforceError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_enforcer`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetEnforcerError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_enforcers`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetEnforcersError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`update_enforcer`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerUpdateEnforcerError {
    UnknownValue(serde_json::Value),
}


/// add enforcer
pub async fn add_enforcer(configuration: &configuration::Configuration, params: ApiControllerAddEnforcerParams) -> Result<models::Enforcer, Error<ApiControllerAddEnforcerError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let enforcer = params.enforcer;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/add-enforcer", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&enforcer);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerAddEnforcerError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// Call Casbin BatchEnforce API
pub async fn batch_enforce(configuration: &configuration::Configuration, params: ApiControllerBatchEnforceParams) -> Result<models::ControllersResponse, Error<ApiControllerBatchEnforceError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;
    let permission_id = params.permission_id;
    let model_id = params.model_id;
    let owner = params.owner;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/batch-enforce", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_str) = permission_id {
        local_var_req_builder = local_var_req_builder.query(&[("permissionId", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = model_id {
        local_var_req_builder = local_var_req_builder.query(&[("modelId", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = owner {
        local_var_req_builder = local_var_req_builder.query(&[("owner", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerBatchEnforceError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// delete enforcer
pub async fn delete_enforcer(configuration: &configuration::Configuration, params: ApiControllerDeleteEnforcerParams) -> Result<models::Enforcer, Error<ApiControllerDeleteEnforcerError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/delete-enforcer", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerDeleteEnforcerError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// Call Casbin Enforce API
pub async fn enforce(configuration: &configuration::Configuration, params: ApiControllerEnforceParams) -> Result<models::ControllersResponse, Error<ApiControllerEnforceError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;
    let permission_id = params.permission_id;
    let model_id = params.model_id;
    let resource_id = params.resource_id;
    let owner = params.owner;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/enforce", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_str) = permission_id {
        local_var_req_builder = local_var_req_builder.query(&[("permissionId", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = model_id {
        local_var_req_builder = local_var_req_builder.query(&[("modelId", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = resource_id {
        local_var_req_builder = local_var_req_builder.query(&[("resourceId", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = owner {
        local_var_req_builder = local_var_req_builder.query(&[("owner", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerEnforceError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get enforcer
pub async fn get_enforcer(configuration: &configuration::Configuration, params: ApiControllerGetEnforcerParams) -> Result<models::Enforcer, Error<ApiControllerGetEnforcerError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-enforcer", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetEnforcerError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get enforcers
pub async fn get_enforcers(configuration: &configuration::Configuration, params: ApiControllerGetEnforcersParams) -> Result<Vec<models::Enforcer>, Error<ApiControllerGetEnforcersError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let owner = params.owner;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-enforcers", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("owner", &owner.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetEnforcersError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// update enforcer
pub async fn update_enforcer(configuration: &configuration::Configuration, params: ApiControllerUpdateEnforcerParams) -> Result<models::Enforcer, Error<ApiControllerUpdateEnforcerError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;
    let enforcer = params.enforcer;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/update-enforcer", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&enforcer);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerUpdateEnforcerError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

