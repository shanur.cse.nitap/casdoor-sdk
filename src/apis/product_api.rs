

use reqwest;

use crate::{apis::ResponseContent, models};
use super::{Error, configuration};

/// struct for passing parameters to the method [`add_product`]
#[derive(Clone, Debug)]
pub struct ApiControllerAddProductParams {
    /// The details of the product
    pub body: models::Product
}

/// struct for passing parameters to the method [`buy_product`]
#[derive(Clone, Debug)]
pub struct ApiControllerBuyProductParams {
    /// The id ( owner/name ) of the product
    pub id: String,
    /// The name of the provider
    pub provider_name: String
}

/// struct for passing parameters to the method [`delete_product`]
#[derive(Clone, Debug)]
pub struct ApiControllerDeleteProductParams {
    /// The details of the product
    pub body: models::Product
}

/// struct for passing parameters to the method [`get_product`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetProductParams {
    /// The id ( owner/name ) of the product
    pub id: String
}

/// struct for passing parameters to the method [`get_products`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetProductsParams {
    /// The owner of products
    pub owner: String
}

/// struct for passing parameters to the method [`update_product`]
#[derive(Clone, Debug)]
pub struct ApiControllerUpdateProductParams {
    /// The id ( owner/name ) of the product
    pub id: String,
    /// The details of the product
    pub body: models::Product
}


/// struct for typed errors of method [`add_product`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerAddProductError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`buy_product`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerBuyProductError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`delete_product`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerDeleteProductError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_product`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetProductError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_products`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetProductsError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`update_product`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerUpdateProductError {
    UnknownValue(serde_json::Value),
}


/// add product
pub async fn add_product(configuration: &configuration::Configuration, params: ApiControllerAddProductParams) -> Result<models::ControllersResponse, Error<ApiControllerAddProductError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/add-product", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerAddProductError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// buy product
pub async fn buy_product(configuration: &configuration::Configuration, params: ApiControllerBuyProductParams) -> Result<models::ControllersResponse, Error<ApiControllerBuyProductError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;
    let provider_name = params.provider_name;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/buy-product", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("providerName", &provider_name.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerBuyProductError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// delete product
pub async fn delete_product(configuration: &configuration::Configuration, params: ApiControllerDeleteProductParams) -> Result<models::ControllersResponse, Error<ApiControllerDeleteProductError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/delete-product", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerDeleteProductError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get product
pub async fn get_product(configuration: &configuration::Configuration, params: ApiControllerGetProductParams) -> Result<models::Product, Error<ApiControllerGetProductError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-product", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetProductError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get products
pub async fn get_products(configuration: &configuration::Configuration, params: ApiControllerGetProductsParams) -> Result<Vec<models::Product>, Error<ApiControllerGetProductsError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let owner = params.owner;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-products", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("owner", &owner.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetProductsError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// update product
pub async fn update_product(configuration: &configuration::Configuration, params: ApiControllerUpdateProductParams) -> Result<models::ControllersResponse, Error<ApiControllerUpdateProductError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/update-product", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerUpdateProductError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

