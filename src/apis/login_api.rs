

use reqwest;

use crate::{apis::ResponseContent, models};
use super::{Error, configuration};

/// struct for passing parameters to the method [`get_application_login`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetApplicationLoginParams {
    /// client id
    pub client_id: String,
    /// response type
    pub response_type: String,
    /// redirect uri
    pub redirect_uri: String,
    /// scope
    pub scope: String,
    /// state
    pub state: String
}

/// struct for passing parameters to the method [`introspect_token`]
#[derive(Clone, Debug)]
pub struct ApiControllerIntrospectTokenParams {
    /// access_token's value or refresh_token's value
    pub token: String,
    /// the token type access_token or refresh_token
    pub token_type_hint: String
}

/// struct for passing parameters to the method [`login`]
#[derive(Clone, Debug)]
pub struct ApiControllerLoginParams {
    /// clientId
    pub client_id: String,
    /// responseType
    pub response_type: String,
    /// redirectUri
    pub redirect_uri: String,
    /// Login information
    pub form: serde_json::Value,
    /// scope
    pub scope: Option<String>,
    /// state
    pub state: Option<String>,
    /// nonce
    pub nonce: Option<String>,
    /// code_challenge_method
    pub code_challenge_method: Option<String>,
    /// code_challenge
    pub code_challenge: Option<String>
}

/// struct for passing parameters to the method [`logout`]
#[derive(Clone, Debug)]
pub struct ApiControllerLogoutParams {
    /// id_token_hint
    pub id_token_hint: Option<String>,
    /// post_logout_redirect_uri
    pub post_logout_redirect_uri: Option<String>,
    /// state
    pub state: Option<String>
}

/// struct for passing parameters to the method [`signup`]
#[derive(Clone, Debug)]
pub struct ApiControllerSignupParams {
    /// The username to sign up
    pub username: String,
    /// The password
    pub password: String
}

/// struct for passing parameters to the method [`web_authn_signin_begin`]
#[derive(Clone, Debug)]
pub struct ApiControllerWebAuthnSigninBeginParams {
    /// owner
    pub owner: String,
    /// name
    pub name: String
}

/// struct for passing parameters to the method [`web_authn_signin_finish`]
#[derive(Clone, Debug)]
pub struct ApiControllerWebAuthnSigninFinishParams {
    /// authenticator assertion Response
    pub body: serde_json::Value
}


/// struct for typed errors of method [`get_application_login`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetApplicationLoginError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_captcha`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetCaptchaError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`introspect_token`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerIntrospectTokenError {
    Status400(models::TokenError),
    Status401(models::TokenError),
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`login`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerLoginError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`logout`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerLogoutError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`signup`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerSignupError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`unlink`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerUnlinkError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`web_authn_signin_begin`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerWebAuthnSigninBeginError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`web_authn_signin_finish`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerWebAuthnSigninFinishError {
    UnknownValue(serde_json::Value),
}


/// get application login
pub async fn get_application_login(configuration: &configuration::Configuration, params: ApiControllerGetApplicationLoginParams) -> Result<models::ControllersResponse, Error<ApiControllerGetApplicationLoginError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let client_id = params.client_id;
    let response_type = params.response_type;
    let redirect_uri = params.redirect_uri;
    let scope = params.scope;
    let state = params.state;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-app-login", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("clientId", &client_id.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("responseType", &response_type.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("redirectUri", &redirect_uri.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("scope", &scope.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("state", &state.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetApplicationLoginError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

pub async fn get_captcha(configuration: &configuration::Configuration) -> Result<models::Userinfo, Error<ApiControllerGetCaptchaError>> {
    let local_var_configuration = configuration;

    // unbox the parameters


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-captcha", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetCaptchaError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// The introspection endpoint is an OAuth 2.0 endpoint that takes a
pub async fn introspect_token(configuration: &configuration::Configuration, params: ApiControllerIntrospectTokenParams) -> Result<models::IntrospectionResponse, Error<ApiControllerIntrospectTokenError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let token = params.token;
    let token_type_hint = params.token_type_hint;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/login/oauth/introspect", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    let mut local_var_form = reqwest::multipart::Form::new();
    local_var_form = local_var_form.text("token", token.to_string());
    local_var_form = local_var_form.text("token_type_hint", token_type_hint.to_string());
    local_var_req_builder = local_var_req_builder.multipart(local_var_form);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerIntrospectTokenError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// login
pub async fn login(configuration: &configuration::Configuration, params: ApiControllerLoginParams) -> Result<models::ControllersResponse, Error<ApiControllerLoginError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let client_id = params.client_id;
    let response_type = params.response_type;
    let redirect_uri = params.redirect_uri;
    let form = params.form;
    let scope = params.scope;
    let state = params.state;
    let nonce = params.nonce;
    let code_challenge_method = params.code_challenge_method;
    let code_challenge = params.code_challenge;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/login", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("clientId", &client_id.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("responseType", &response_type.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("redirectUri", &redirect_uri.to_string())]);
    if let Some(ref local_var_str) = scope {
        local_var_req_builder = local_var_req_builder.query(&[("scope", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = state {
        local_var_req_builder = local_var_req_builder.query(&[("state", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = nonce {
        local_var_req_builder = local_var_req_builder.query(&[("nonce", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = code_challenge_method {
        local_var_req_builder = local_var_req_builder.query(&[("code_challenge_method", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = code_challenge {
        local_var_req_builder = local_var_req_builder.query(&[("code_challenge", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&form);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerLoginError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// logout the current user
pub async fn logout(configuration: &configuration::Configuration, params: ApiControllerLogoutParams) -> Result<models::ControllersResponse, Error<ApiControllerLogoutError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id_token_hint = params.id_token_hint;
    let post_logout_redirect_uri = params.post_logout_redirect_uri;
    let state = params.state;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/logout", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_str) = id_token_hint {
        local_var_req_builder = local_var_req_builder.query(&[("id_token_hint", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = post_logout_redirect_uri {
        local_var_req_builder = local_var_req_builder.query(&[("post_logout_redirect_uri", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_str) = state {
        local_var_req_builder = local_var_req_builder.query(&[("state", &local_var_str.to_string())]);
    }
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerLogoutError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// sign up a new user
pub async fn signup(configuration: &configuration::Configuration, params: ApiControllerSignupParams) -> Result<models::ControllersResponse, Error<ApiControllerSignupError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let username = params.username;
    let password = params.password;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/signup", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    let mut local_var_form = reqwest::multipart::Form::new();
    local_var_form = local_var_form.text("username", username.to_string());
    local_var_form = local_var_form.text("password", password.to_string());
    local_var_req_builder = local_var_req_builder.multipart(local_var_form);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerSignupError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

pub async fn unlink(configuration: &configuration::Configuration) -> Result<models::Userinfo, Error<ApiControllerUnlinkError>> {
    let local_var_configuration = configuration;

    // unbox the parameters


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/unlink", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerUnlinkError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// WebAuthn Login Flow 1st stage
pub async fn web_authn_signin_begin(configuration: &configuration::Configuration, params: ApiControllerWebAuthnSigninBeginParams) -> Result<serde_json::Value, Error<ApiControllerWebAuthnSigninBeginError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let owner = params.owner;
    let name = params.name;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/webauthn/signin/begin", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("owner", &owner.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("name", &name.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerWebAuthnSigninBeginError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// WebAuthn Login Flow 2nd stage
pub async fn web_authn_signin_finish(configuration: &configuration::Configuration, params: ApiControllerWebAuthnSigninFinishParams) -> Result<models::ControllersResponse, Error<ApiControllerWebAuthnSigninFinishError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/webauthn/signin/finish", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerWebAuthnSigninFinishError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

