

use reqwest;

use crate::{apis::ResponseContent, models};
use super::{Error, configuration};

/// struct for passing parameters to the method [`send_email`]
#[derive(Clone, Debug)]
pub struct ApiControllerSendEmailParams {
    /// The clientId of the application
    pub client_id: String,
    /// The clientSecret of the application
    pub client_secret: String,
    /// Details of the email request
    pub from: models::ControllersEmailForm
}

/// struct for passing parameters to the method [`send_notification`]
#[derive(Clone, Debug)]
pub struct ApiControllerSendNotificationParams {
    /// Details of the notification request
    pub from: models::ControllersNotificationForm
}

/// struct for passing parameters to the method [`send_sms`]
#[derive(Clone, Debug)]
pub struct ApiControllerSendSmsParams {
    /// The clientId of the application
    pub client_id: String,
    /// The clientSecret of the application
    pub client_secret: String,
    /// Details of the sms request
    pub from: models::ControllersSmsForm
}


/// struct for typed errors of method [`send_email`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerSendEmailError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`send_notification`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerSendNotificationError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`send_sms`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerSendSmsError {
    UnknownValue(serde_json::Value),
}


/// This API is not for Casdoor frontend to call, it is for Casdoor SDKs.
pub async fn send_email(configuration: &configuration::Configuration, params: ApiControllerSendEmailParams) -> Result<models::ControllersResponse, Error<ApiControllerSendEmailError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let client_id = params.client_id;
    let client_secret = params.client_secret;
    let from = params.from;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/send-email", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("clientId", &client_id.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("clientSecret", &client_secret.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&from);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerSendEmailError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// This API is not for Casdoor frontend to call, it is for Casdoor SDKs.
pub async fn send_notification(configuration: &configuration::Configuration, params: ApiControllerSendNotificationParams) -> Result<models::ControllersResponse, Error<ApiControllerSendNotificationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let from = params.from;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/send-notification", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&from);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerSendNotificationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// This API is not for Casdoor frontend to call, it is for Casdoor SDKs.
pub async fn send_sms(configuration: &configuration::Configuration, params: ApiControllerSendSmsParams) -> Result<models::ControllersResponse, Error<ApiControllerSendSmsError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let client_id = params.client_id;
    let client_secret = params.client_secret;
    let from = params.from;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/send-sms", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("clientId", &client_id.to_string())]);
    local_var_req_builder = local_var_req_builder.query(&[("clientSecret", &client_secret.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&from);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerSendSmsError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

