

use reqwest;

use crate::{apis::ResponseContent, models};
use super::{Error, configuration};

/// struct for passing parameters to the method [`add_application`]
#[derive(Clone, Debug)]
pub struct ApiControllerAddApplicationParams {
    /// The details of the application
    pub body: models::Application
}

/// struct for passing parameters to the method [`delete_application`]
#[derive(Clone, Debug)]
pub struct ApiControllerDeleteApplicationParams {
    /// The details of the application
    pub body: models::Application
}

/// struct for passing parameters to the method [`get_application`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetApplicationParams {
    /// The id ( owner/name ) of the application.
    pub id: String
}

/// struct for passing parameters to the method [`get_applications`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetApplicationsParams {
    /// The owner of applications.
    pub owner: String
}

/// struct for passing parameters to the method [`get_organization_applications`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetOrganizationApplicationsParams {
    /// The organization name
    pub organization: String
}

/// struct for passing parameters to the method [`get_user_application`]
#[derive(Clone, Debug)]
pub struct ApiControllerGetUserApplicationParams {
    /// The id ( owner/name ) of the user
    pub id: String
}

/// struct for passing parameters to the method [`update_application`]
#[derive(Clone, Debug)]
pub struct ApiControllerUpdateApplicationParams {
    /// The id ( owner/name ) of the application
    pub id: String,
    /// The details of the application
    pub body: models::Application
}


/// struct for typed errors of method [`add_application`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerAddApplicationError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`delete_application`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerDeleteApplicationError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_application`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetApplicationError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_applications`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetApplicationsError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_organization_applications`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetOrganizationApplicationsError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`get_user_application`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerGetUserApplicationError {
    UnknownValue(serde_json::Value),
}

/// struct for typed errors of method [`update_application`]
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(untagged)]
pub enum ApiControllerUpdateApplicationError {
    UnknownValue(serde_json::Value),
}


/// add an application
pub async fn add_application(configuration: &configuration::Configuration, params: ApiControllerAddApplicationParams) -> Result<models::ControllersResponse, Error<ApiControllerAddApplicationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/add-application", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerAddApplicationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// delete an application
pub async fn delete_application(configuration: &configuration::Configuration, params: ApiControllerDeleteApplicationParams) -> Result<models::ControllersResponse, Error<ApiControllerDeleteApplicationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/delete-application", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerDeleteApplicationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get the detail of an application
pub async fn get_application(configuration: &configuration::Configuration, params: ApiControllerGetApplicationParams) -> Result<models::Application, Error<ApiControllerGetApplicationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-application", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetApplicationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get all applications
pub async fn get_applications(configuration: &configuration::Configuration, params: ApiControllerGetApplicationsParams) -> Result<Vec<models::Application>, Error<ApiControllerGetApplicationsError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let owner = params.owner;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-applications", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("owner", &owner.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetApplicationsError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get the detail of the organization's application
pub async fn get_organization_applications(configuration: &configuration::Configuration, params: ApiControllerGetOrganizationApplicationsParams) -> Result<Vec<models::Application>, Error<ApiControllerGetOrganizationApplicationsError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let organization = params.organization;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-organization-applications", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("organization", &organization.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetOrganizationApplicationsError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// get the detail of the user's application
pub async fn get_user_application(configuration: &configuration::Configuration, params: ApiControllerGetUserApplicationParams) -> Result<models::Application, Error<ApiControllerGetUserApplicationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/get-user-application", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::GET, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerGetUserApplicationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

/// update an application
pub async fn update_application(configuration: &configuration::Configuration, params: ApiControllerUpdateApplicationParams) -> Result<models::ControllersResponse, Error<ApiControllerUpdateApplicationError>> {
    let local_var_configuration = configuration;

    // unbox the parameters
    let id = params.id;
    let body = params.body;


    let local_var_client = &local_var_configuration.client;

    let local_var_uri_str = format!("{}/api/update-application", local_var_configuration.base_path);
    let mut local_var_req_builder = local_var_client.request(reqwest::Method::POST, local_var_uri_str.as_str());

    local_var_req_builder = local_var_req_builder.query(&[("id", &id.to_string())]);
    if let Some(ref local_var_user_agent) = local_var_configuration.user_agent {
        local_var_req_builder = local_var_req_builder.header(reqwest::header::USER_AGENT, local_var_user_agent.clone());
    }
    local_var_req_builder = local_var_req_builder.json(&body);

    let local_var_req = local_var_req_builder.build()?;
    let local_var_resp = local_var_client.execute(local_var_req).await?;

    let local_var_status = local_var_resp.status();
    let local_var_content = local_var_resp.text().await?;

    if !local_var_status.is_client_error() && !local_var_status.is_server_error() {
        serde_json::from_str(&local_var_content).map_err(Error::from)
    } else {
        let local_var_entity: Option<ApiControllerUpdateApplicationError> = serde_json::from_str(&local_var_content).ok();
        let local_var_error = ResponseContent { status: local_var_status, content: local_var_content, entity: local_var_entity };
        Err(Error::ResponseError(local_var_error))
    }
}

