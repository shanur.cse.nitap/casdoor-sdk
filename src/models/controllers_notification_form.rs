
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct ControllersNotificationForm {
    #[serde(rename = "content", skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,
}

impl ControllersNotificationForm {
    pub fn new() -> ControllersNotificationForm {
        ControllersNotificationForm {
            content: None,
        }
    }
}

