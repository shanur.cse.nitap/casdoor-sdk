
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct ControllersLaravelResponse {
    #[serde(rename = "created_at", skip_serializing_if = "Option::is_none")]
    pub created_at: Option<String>,
    #[serde(rename = "email", skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(rename = "email_verified_at", skip_serializing_if = "Option::is_none")]
    pub email_verified_at: Option<String>,
    #[serde(rename = "id", skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "updated_at", skip_serializing_if = "Option::is_none")]
    pub updated_at: Option<String>,
}

impl ControllersLaravelResponse {
    pub fn new() -> ControllersLaravelResponse {
        ControllersLaravelResponse {
            created_at: None,
            email: None,
            email_verified_at: None,
            id: None,
            name: None,
            updated_at: None,
        }
    }
}

