
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct User {
    #[serde(rename = "accessKey", skip_serializing_if = "Option::is_none")]
    pub access_key: Option<String>,
    #[serde(rename = "accessSecret", skip_serializing_if = "Option::is_none")]
    pub access_secret: Option<String>,
    #[serde(rename = "address", skip_serializing_if = "Option::is_none")]
    pub address: Option<Vec<String>>,
    #[serde(rename = "adfs", skip_serializing_if = "Option::is_none")]
    pub adfs: Option<String>,
    #[serde(rename = "affiliation", skip_serializing_if = "Option::is_none")]
    pub affiliation: Option<String>,
    #[serde(rename = "alipay", skip_serializing_if = "Option::is_none")]
    pub alipay: Option<String>,
    #[serde(rename = "amazon", skip_serializing_if = "Option::is_none")]
    pub amazon: Option<String>,
    #[serde(rename = "apple", skip_serializing_if = "Option::is_none")]
    pub apple: Option<String>,
    #[serde(rename = "auth0", skip_serializing_if = "Option::is_none")]
    pub auth0: Option<String>,
    #[serde(rename = "avatar", skip_serializing_if = "Option::is_none")]
    pub avatar: Option<String>,
    #[serde(rename = "avatarType", skip_serializing_if = "Option::is_none")]
    pub avatar_type: Option<String>,
    #[serde(rename = "azuread", skip_serializing_if = "Option::is_none")]
    pub azuread: Option<String>,
    #[serde(rename = "azureadb2c", skip_serializing_if = "Option::is_none")]
    pub azureadb2c: Option<String>,
    #[serde(rename = "baidu", skip_serializing_if = "Option::is_none")]
    pub baidu: Option<String>,
    #[serde(rename = "battlenet", skip_serializing_if = "Option::is_none")]
    pub battlenet: Option<String>,
    #[serde(rename = "bilibili", skip_serializing_if = "Option::is_none")]
    pub bilibili: Option<String>,
    #[serde(rename = "bio", skip_serializing_if = "Option::is_none")]
    pub bio: Option<String>,
    #[serde(rename = "birthday", skip_serializing_if = "Option::is_none")]
    pub birthday: Option<String>,
    #[serde(rename = "bitbucket", skip_serializing_if = "Option::is_none")]
    pub bitbucket: Option<String>,
    #[serde(rename = "box", skip_serializing_if = "Option::is_none")]
    pub r#box: Option<String>,
    #[serde(rename = "casdoor", skip_serializing_if = "Option::is_none")]
    pub casdoor: Option<String>,
    #[serde(rename = "cloudfoundry", skip_serializing_if = "Option::is_none")]
    pub cloudfoundry: Option<String>,
    #[serde(rename = "countryCode", skip_serializing_if = "Option::is_none")]
    pub country_code: Option<String>,
    #[serde(rename = "createdIp", skip_serializing_if = "Option::is_none")]
    pub created_ip: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "custom", skip_serializing_if = "Option::is_none")]
    pub custom: Option<String>,
    #[serde(rename = "dailymotion", skip_serializing_if = "Option::is_none")]
    pub dailymotion: Option<String>,
    #[serde(rename = "deezer", skip_serializing_if = "Option::is_none")]
    pub deezer: Option<String>,
    #[serde(rename = "digitalocean", skip_serializing_if = "Option::is_none")]
    pub digitalocean: Option<String>,
    #[serde(rename = "dingtalk", skip_serializing_if = "Option::is_none")]
    pub dingtalk: Option<String>,
    #[serde(rename = "discord", skip_serializing_if = "Option::is_none")]
    pub discord: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "deletedTime", skip_serializing_if = "Option::is_none")]
    pub deleted_time: Option<String>,
    #[serde(rename = "douyin", skip_serializing_if = "Option::is_none")]
    pub douyin: Option<String>,
    #[serde(rename = "dropbox", skip_serializing_if = "Option::is_none")]
    pub dropbox: Option<String>,
    #[serde(rename = "education", skip_serializing_if = "Option::is_none")]
    pub education: Option<String>,
    #[serde(rename = "email", skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(rename = "emailVerified", skip_serializing_if = "Option::is_none")]
    pub email_verified: Option<bool>,
    #[serde(rename = "eveonline", skip_serializing_if = "Option::is_none")]
    pub eveonline: Option<String>,
    #[serde(rename = "externalId", skip_serializing_if = "Option::is_none")]
    pub external_id: Option<String>,
    #[serde(rename = "facebook", skip_serializing_if = "Option::is_none")]
    pub facebook: Option<String>,
    #[serde(rename = "firstName", skip_serializing_if = "Option::is_none")]
    pub first_name: Option<String>,
    #[serde(rename = "fitbit", skip_serializing_if = "Option::is_none")]
    pub fitbit: Option<String>,
    #[serde(rename = "gender", skip_serializing_if = "Option::is_none")]
    pub gender: Option<String>,
    #[serde(rename = "gitea", skip_serializing_if = "Option::is_none")]
    pub gitea: Option<String>,
    #[serde(rename = "gitee", skip_serializing_if = "Option::is_none")]
    pub gitee: Option<String>,
    #[serde(rename = "github", skip_serializing_if = "Option::is_none")]
    pub github: Option<String>,
    #[serde(rename = "gitlab", skip_serializing_if = "Option::is_none")]
    pub gitlab: Option<String>,
    #[serde(rename = "google", skip_serializing_if = "Option::is_none")]
    pub google: Option<String>,
    #[serde(rename = "groups", skip_serializing_if = "Option::is_none")]
    pub groups: Option<Vec<String>>,
    #[serde(rename = "hash", skip_serializing_if = "Option::is_none")]
    pub hash: Option<String>,
    #[serde(rename = "heroku", skip_serializing_if = "Option::is_none")]
    pub heroku: Option<String>,
    #[serde(rename = "homepage", skip_serializing_if = "Option::is_none")]
    pub homepage: Option<String>,
    #[serde(rename = "id", skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "idCard", skip_serializing_if = "Option::is_none")]
    pub id_card: Option<String>,
    #[serde(rename = "idCardType", skip_serializing_if = "Option::is_none")]
    pub id_card_type: Option<String>,
    #[serde(rename = "influxcloud", skip_serializing_if = "Option::is_none")]
    pub influxcloud: Option<String>,
    #[serde(rename = "infoflow", skip_serializing_if = "Option::is_none")]
    pub infoflow: Option<String>,
    #[serde(rename = "instagram", skip_serializing_if = "Option::is_none")]
    pub instagram: Option<String>,
    #[serde(rename = "intercom", skip_serializing_if = "Option::is_none")]
    pub intercom: Option<String>,
    #[serde(rename = "isAdmin", skip_serializing_if = "Option::is_none")]
    pub is_admin: Option<bool>,
    #[serde(rename = "isDefaultAvatar", skip_serializing_if = "Option::is_none")]
    pub is_default_avatar: Option<bool>,
    #[serde(rename = "isDeleted", skip_serializing_if = "Option::is_none")]
    pub is_deleted: Option<bool>,
    #[serde(rename = "isForbidden", skip_serializing_if = "Option::is_none")]
    pub is_forbidden: Option<bool>,
    #[serde(rename = "isOnline", skip_serializing_if = "Option::is_none")]
    pub is_online: Option<bool>,
    #[serde(rename = "kakao", skip_serializing_if = "Option::is_none")]
    pub kakao: Option<String>,
    #[serde(rename = "karma", skip_serializing_if = "Option::is_none")]
    pub karma: Option<i64>,
    #[serde(rename = "language", skip_serializing_if = "Option::is_none")]
    pub language: Option<String>,
    #[serde(rename = "lark", skip_serializing_if = "Option::is_none")]
    pub lark: Option<String>,
    #[serde(rename = "lastName", skip_serializing_if = "Option::is_none")]
    pub last_name: Option<String>,
    #[serde(rename = "lastSigninIp", skip_serializing_if = "Option::is_none")]
    pub last_signin_ip: Option<String>,
    #[serde(rename = "lastSigninTime", skip_serializing_if = "Option::is_none")]
    pub last_signin_time: Option<String>,
    #[serde(rename = "lastSigninWrongTime", skip_serializing_if = "Option::is_none")]
    pub last_signin_wrong_time: Option<String>,
    #[serde(rename = "lastfm", skip_serializing_if = "Option::is_none")]
    pub lastfm: Option<String>,
    #[serde(rename = "ldap", skip_serializing_if = "Option::is_none")]
    pub ldap: Option<String>,
    #[serde(rename = "line", skip_serializing_if = "Option::is_none")]
    pub line: Option<String>,
    #[serde(rename = "linkedin", skip_serializing_if = "Option::is_none")]
    pub linkedin: Option<String>,
    #[serde(rename = "location", skip_serializing_if = "Option::is_none")]
    pub location: Option<String>,
    #[serde(rename = "mailru", skip_serializing_if = "Option::is_none")]
    pub mailru: Option<String>,
    #[serde(rename = "managedAccounts", skip_serializing_if = "Option::is_none")]
    pub managed_accounts: Option<Vec<models::ManagedAccount>>,
    #[serde(rename = "meetup", skip_serializing_if = "Option::is_none")]
    pub meetup: Option<String>,
    #[serde(rename = "metamask", skip_serializing_if = "Option::is_none")]
    pub metamask: Option<String>,
    #[serde(rename = "mfaEmailEnabled", skip_serializing_if = "Option::is_none")]
    pub mfa_email_enabled: Option<bool>,
    #[serde(rename = "mfaPhoneEnabled", skip_serializing_if = "Option::is_none")]
    pub mfa_phone_enabled: Option<bool>,
    #[serde(rename = "microsoftonline", skip_serializing_if = "Option::is_none")]
    pub microsoftonline: Option<String>,
    #[serde(rename = "multiFactorAuths", skip_serializing_if = "Option::is_none")]
    pub multi_factor_auths: Option<Vec<models::MfaProps>>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "naver", skip_serializing_if = "Option::is_none")]
    pub naver: Option<String>,
    #[serde(rename = "nextcloud", skip_serializing_if = "Option::is_none")]
    pub nextcloud: Option<String>,
    #[serde(rename = "okta", skip_serializing_if = "Option::is_none")]
    pub okta: Option<String>,
    #[serde(rename = "onedrive", skip_serializing_if = "Option::is_none")]
    pub onedrive: Option<String>,
    #[serde(rename = "oura", skip_serializing_if = "Option::is_none")]
    pub oura: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "password", skip_serializing_if = "Option::is_none")]
    pub password: Option<String>,
    #[serde(rename = "passwordSalt", skip_serializing_if = "Option::is_none")]
    pub password_salt: Option<String>,
    #[serde(rename = "passwordType", skip_serializing_if = "Option::is_none")]
    pub password_type: Option<String>,
    #[serde(rename = "patreon", skip_serializing_if = "Option::is_none")]
    pub patreon: Option<String>,
    #[serde(rename = "paypal", skip_serializing_if = "Option::is_none")]
    pub paypal: Option<String>,
    #[serde(rename = "permanentAvatar", skip_serializing_if = "Option::is_none")]
    pub permanent_avatar: Option<String>,
    #[serde(rename = "permissions", skip_serializing_if = "Option::is_none")]
    pub permissions: Option<Vec<models::Permission>>,
    #[serde(rename = "phone", skip_serializing_if = "Option::is_none")]
    pub phone: Option<String>,
    #[serde(rename = "preHash", skip_serializing_if = "Option::is_none")]
    pub pre_hash: Option<String>,
    #[serde(rename = "preferredMfaType", skip_serializing_if = "Option::is_none")]
    pub preferred_mfa_type: Option<String>,
    #[serde(rename = "properties", skip_serializing_if = "Option::is_none")]
    pub properties: Option<serde_json::Value>,
    #[serde(rename = "qq", skip_serializing_if = "Option::is_none")]
    pub qq: Option<String>,
    #[serde(rename = "ranking", skip_serializing_if = "Option::is_none")]
    pub ranking: Option<i64>,
    #[serde(rename = "recoveryCodes", skip_serializing_if = "Option::is_none")]
    pub recovery_codes: Option<Vec<String>>,
    #[serde(rename = "region", skip_serializing_if = "Option::is_none")]
    pub region: Option<String>,
    #[serde(rename = "roles", skip_serializing_if = "Option::is_none")]
    pub roles: Option<Vec<models::Role>>,
    #[serde(rename = "salesforce", skip_serializing_if = "Option::is_none")]
    pub salesforce: Option<String>,
    #[serde(rename = "score", skip_serializing_if = "Option::is_none")]
    pub score: Option<i64>,
    #[serde(rename = "shopify", skip_serializing_if = "Option::is_none")]
    pub shopify: Option<String>,
    #[serde(rename = "signinWrongTimes", skip_serializing_if = "Option::is_none")]
    pub signin_wrong_times: Option<i64>,
    #[serde(rename = "signupApplication", skip_serializing_if = "Option::is_none")]
    pub signup_application: Option<String>,
    #[serde(rename = "slack", skip_serializing_if = "Option::is_none")]
    pub slack: Option<String>,
    #[serde(rename = "soundcloud", skip_serializing_if = "Option::is_none")]
    pub soundcloud: Option<String>,
    #[serde(rename = "spotify", skip_serializing_if = "Option::is_none")]
    pub spotify: Option<String>,
    #[serde(rename = "steam", skip_serializing_if = "Option::is_none")]
    pub steam: Option<String>,
    #[serde(rename = "strava", skip_serializing_if = "Option::is_none")]
    pub strava: Option<String>,
    #[serde(rename = "stripe", skip_serializing_if = "Option::is_none")]
    pub stripe: Option<String>,
    #[serde(rename = "tag", skip_serializing_if = "Option::is_none")]
    pub tag: Option<String>,
    #[serde(rename = "tiktok", skip_serializing_if = "Option::is_none")]
    pub tiktok: Option<String>,
    #[serde(rename = "title", skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(rename = "totpSecret", skip_serializing_if = "Option::is_none")]
    pub totp_secret: Option<String>,
    #[serde(rename = "tumblr", skip_serializing_if = "Option::is_none")]
    pub tumblr: Option<String>,
    #[serde(rename = "twitch", skip_serializing_if = "Option::is_none")]
    pub twitch: Option<String>,
    #[serde(rename = "twitter", skip_serializing_if = "Option::is_none")]
    pub twitter: Option<String>,
    #[serde(rename = "type", skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
    #[serde(rename = "typetalk", skip_serializing_if = "Option::is_none")]
    pub typetalk: Option<String>,
    #[serde(rename = "uber", skip_serializing_if = "Option::is_none")]
    pub uber: Option<String>,
    #[serde(rename = "updatedTime", skip_serializing_if = "Option::is_none")]
    pub updated_time: Option<String>,
    #[serde(rename = "vk", skip_serializing_if = "Option::is_none")]
    pub vk: Option<String>,
    #[serde(rename = "web3onboard", skip_serializing_if = "Option::is_none")]
    pub web3onboard: Option<String>,
    #[serde(rename = "webauthnCredentials", skip_serializing_if = "Option::is_none")]
    pub webauthn_credentials: Option<Vec<serde_json::Value>>,
    #[serde(rename = "wechat", skip_serializing_if = "Option::is_none")]
    pub wechat: Option<String>,
    #[serde(rename = "wecom", skip_serializing_if = "Option::is_none")]
    pub wecom: Option<String>,
    #[serde(rename = "weibo", skip_serializing_if = "Option::is_none")]
    pub weibo: Option<String>,
    #[serde(rename = "wepay", skip_serializing_if = "Option::is_none")]
    pub wepay: Option<String>,
    #[serde(rename = "xero", skip_serializing_if = "Option::is_none")]
    pub xero: Option<String>,
    #[serde(rename = "yahoo", skip_serializing_if = "Option::is_none")]
    pub yahoo: Option<String>,
    #[serde(rename = "yammer", skip_serializing_if = "Option::is_none")]
    pub yammer: Option<String>,
    #[serde(rename = "yandex", skip_serializing_if = "Option::is_none")]
    pub yandex: Option<String>,
    #[serde(rename = "zoom", skip_serializing_if = "Option::is_none")]
    pub zoom: Option<String>,
}

impl User {
    pub fn new() -> User {
        User {
            access_key: None,
            access_secret: None,
            address: None,
            adfs: None,
            affiliation: None,
            alipay: None,
            amazon: None,
            apple: None,
            auth0: None,
            avatar: None,
            avatar_type: None,
            azuread: None,
            azureadb2c: None,
            baidu: None,
            battlenet: None,
            bilibili: None,
            bio: None,
            birthday: None,
            bitbucket: None,
            r#box: None,
            casdoor: None,
            cloudfoundry: None,
            country_code: None,
            created_ip: None,
            created_time: None,
            custom: None,
            dailymotion: None,
            deezer: None,
            digitalocean: None,
            dingtalk: None,
            discord: None,
            display_name: None,
            deleted_time: None,
            douyin: None,
            dropbox: None,
            education: None,
            email: None,
            email_verified: None,
            eveonline: None,
            external_id: None,
            facebook: None,
            first_name: None,
            fitbit: None,
            gender: None,
            gitea: None,
            gitee: None,
            github: None,
            gitlab: None,
            google: None,
            groups: None,
            hash: None,
            heroku: None,
            homepage: None,
            id: None,
            id_card: None,
            id_card_type: None,
            influxcloud: None,
            infoflow: None,
            instagram: None,
            intercom: None,
            is_admin: None,
            is_default_avatar: None,
            is_deleted: None,
            is_forbidden: None,
            is_online: None,
            kakao: None,
            karma: None,
            language: None,
            lark: None,
            last_name: None,
            last_signin_ip: None,
            last_signin_time: None,
            last_signin_wrong_time: None,
            lastfm: None,
            ldap: None,
            line: None,
            linkedin: None,
            location: None,
            mailru: None,
            managed_accounts: None,
            meetup: None,
            metamask: None,
            mfa_email_enabled: None,
            mfa_phone_enabled: None,
            microsoftonline: None,
            multi_factor_auths: None,
            name: None,
            naver: None,
            nextcloud: None,
            okta: None,
            onedrive: None,
            oura: None,
            owner: None,
            password: None,
            password_salt: None,
            password_type: None,
            patreon: None,
            paypal: None,
            permanent_avatar: None,
            permissions: None,
            phone: None,
            pre_hash: None,
            preferred_mfa_type: None,
            properties: None,
            qq: None,
            ranking: None,
            recovery_codes: None,
            region: None,
            roles: None,
            salesforce: None,
            score: None,
            shopify: None,
            signin_wrong_times: None,
            signup_application: None,
            slack: None,
            soundcloud: None,
            spotify: None,
            steam: None,
            strava: None,
            stripe: None,
            tag: None,
            tiktok: None,
            title: None,
            totp_secret: None,
            tumblr: None,
            twitch: None,
            twitter: None,
            r#type: None,
            typetalk: None,
            uber: None,
            updated_time: None,
            vk: None,
            web3onboard: None,
            webauthn_credentials: None,
            wechat: None,
            wecom: None,
            weibo: None,
            wepay: None,
            xero: None,
            yahoo: None,
            yammer: None,
            yandex: None,
            zoom: None,
        }
    }
}

