
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct PrometheusInfo {
    #[serde(rename = "apiLatency", skip_serializing_if = "Option::is_none")]
    pub api_latency: Option<Vec<models::HistogramVecInfo>>,
    #[serde(rename = "apiThroughput", skip_serializing_if = "Option::is_none")]
    pub api_throughput: Option<Vec<models::GaugeVecInfo>>,
    #[serde(rename = "totalThroughput", skip_serializing_if = "Option::is_none")]
    pub total_throughput: Option<f64>,
}

impl PrometheusInfo {
    pub fn new() -> PrometheusInfo {
        PrometheusInfo {
            api_latency: None,
            api_throughput: None,
            total_throughput: None,
        }
    }
}

