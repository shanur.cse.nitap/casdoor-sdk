
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct ControllersLdapResp {
    #[serde(rename = "existUuids", skip_serializing_if = "Option::is_none")]
    pub exist_uuids: Option<Vec<String>>,
    #[serde(rename = "users", skip_serializing_if = "Option::is_none")]
    pub users: Option<Vec<models::LdapUser>>,
}

impl ControllersLdapResp {
    pub fn new() -> ControllersLdapResp {
        ControllersLdapResp {
            exist_uuids: None,
            users: None,
        }
    }
}

