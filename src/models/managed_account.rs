
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct ManagedAccount {
    #[serde(rename = "application", skip_serializing_if = "Option::is_none")]
    pub application: Option<String>,
    #[serde(rename = "password", skip_serializing_if = "Option::is_none")]
    pub password: Option<String>,
    #[serde(rename = "signinUrl", skip_serializing_if = "Option::is_none")]
    pub signin_url: Option<String>,
    #[serde(rename = "username", skip_serializing_if = "Option::is_none")]
    pub username: Option<String>,
}

impl ManagedAccount {
    pub fn new() -> ManagedAccount {
        ManagedAccount {
            application: None,
            password: None,
            signin_url: None,
            username: None,
        }
    }
}

