
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct SamlItem {
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "nameFormat", skip_serializing_if = "Option::is_none")]
    pub name_format: Option<String>,
    #[serde(rename = "value", skip_serializing_if = "Option::is_none")]
    pub value: Option<String>,
}

impl SamlItem {
    pub fn new() -> SamlItem {
        SamlItem {
            name: None,
            name_format: None,
            value: None,
        }
    }
}

