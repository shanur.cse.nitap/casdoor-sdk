
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct ThemeData {
    #[serde(rename = "borderRadius", skip_serializing_if = "Option::is_none")]
    pub border_radius: Option<i64>,
    #[serde(rename = "colorPrimary", skip_serializing_if = "Option::is_none")]
    pub color_primary: Option<String>,
    #[serde(rename = "isCompact", skip_serializing_if = "Option::is_none")]
    pub is_compact: Option<bool>,
    #[serde(rename = "isEnabled", skip_serializing_if = "Option::is_none")]
    pub is_enabled: Option<bool>,
    #[serde(rename = "themeType", skip_serializing_if = "Option::is_none")]
    pub theme_type: Option<String>,
}

impl ThemeData {
    pub fn new() -> ThemeData {
        ThemeData {
            border_radius: None,
            color_primary: None,
            is_compact: None,
            is_enabled: None,
            theme_type: None,
        }
    }
}

