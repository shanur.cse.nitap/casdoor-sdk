
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct ControllersLdapSyncResp {
    #[serde(rename = "exist", skip_serializing_if = "Option::is_none")]
    pub exist: Option<Vec<models::LdapUser>>,
    #[serde(rename = "failed", skip_serializing_if = "Option::is_none")]
    pub failed: Option<Vec<models::LdapUser>>,
}

impl ControllersLdapSyncResp {
    pub fn new() -> ControllersLdapSyncResp {
        ControllersLdapSyncResp {
            exist: None,
            failed: None,
        }
    }
}

