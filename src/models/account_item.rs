
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct AccountItem {
    #[serde(rename = "modifyRule", skip_serializing_if = "Option::is_none")]
    pub modify_rule: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "viewRule", skip_serializing_if = "Option::is_none")]
    pub view_rule: Option<String>,
    #[serde(rename = "visible", skip_serializing_if = "Option::is_none")]
    pub visible: Option<bool>,
}

impl AccountItem {
    pub fn new() -> AccountItem {
        AccountItem {
            modify_rule: None,
            name: None,
            view_rule: None,
            visible: None,
        }
    }
}

