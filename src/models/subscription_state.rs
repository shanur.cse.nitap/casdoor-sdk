
use crate::models;

/// 
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum SubscriptionState {
    #[serde(rename = "SubStatePending = \"Pending\"")]
    SubStatePendingEqualDoubleQuotePendingDoubleQuote,
    #[serde(rename = "SubStateError = \"Error\"")]
    SubStateErrorEqualDoubleQuoteErrorDoubleQuote,
    #[serde(rename = "SubStateSuspended = \"Suspended\"")]
    SubStateSuspendedEqualDoubleQuoteSuspendedDoubleQuote,
    #[serde(rename = "SubStateActive = \"Active\"")]
    SubStateActiveEqualDoubleQuoteActiveDoubleQuote,
    #[serde(rename = "SubStateUpcoming = \"Upcoming\"")]
    SubStateUpcomingEqualDoubleQuoteUpcomingDoubleQuote,
    #[serde(rename = "SubStateExpired = \"Expired\"")]
    SubStateExpiredEqualDoubleQuoteExpiredDoubleQuote,

}

impl ToString for SubscriptionState {
    fn to_string(&self) -> String {
        match self {
            Self::SubStatePendingEqualDoubleQuotePendingDoubleQuote => String::from("SubStatePending = \"Pending\""),
            Self::SubStateErrorEqualDoubleQuoteErrorDoubleQuote => String::from("SubStateError = \"Error\""),
            Self::SubStateSuspendedEqualDoubleQuoteSuspendedDoubleQuote => String::from("SubStateSuspended = \"Suspended\""),
            Self::SubStateActiveEqualDoubleQuoteActiveDoubleQuote => String::from("SubStateActive = \"Active\""),
            Self::SubStateUpcomingEqualDoubleQuoteUpcomingDoubleQuote => String::from("SubStateUpcoming = \"Upcoming\""),
            Self::SubStateExpiredEqualDoubleQuoteExpiredDoubleQuote => String::from("SubStateExpired = \"Expired\""),
        }
    }
}

impl Default for SubscriptionState {
    fn default() -> SubscriptionState {
        Self::SubStatePendingEqualDoubleQuotePendingDoubleQuote
    }
}

