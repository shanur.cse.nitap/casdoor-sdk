
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Cert {
    #[serde(rename = "bitSize", skip_serializing_if = "Option::is_none")]
    pub bit_size: Option<i64>,
    #[serde(rename = "certificate", skip_serializing_if = "Option::is_none")]
    pub certificate: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "cryptoAlgorithm", skip_serializing_if = "Option::is_none")]
    pub crypto_algorithm: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "expireInYears", skip_serializing_if = "Option::is_none")]
    pub expire_in_years: Option<i64>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "privateKey", skip_serializing_if = "Option::is_none")]
    pub private_key: Option<String>,
    #[serde(rename = "scope", skip_serializing_if = "Option::is_none")]
    pub scope: Option<String>,
    #[serde(rename = "type", skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
}

impl Cert {
    pub fn new() -> Cert {
        Cert {
            bit_size: None,
            certificate: None,
            created_time: None,
            crypto_algorithm: None,
            display_name: None,
            expire_in_years: None,
            name: None,
            owner: None,
            private_key: None,
            scope: None,
            r#type: None,
        }
    }
}

