
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct SigninMethod {
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "rule", skip_serializing_if = "Option::is_none")]
    pub rule: Option<String>,
}

impl SigninMethod {
    pub fn new() -> SigninMethod {
        SigninMethod {
            display_name: None,
            name: None,
            rule: None,
        }
    }
}

