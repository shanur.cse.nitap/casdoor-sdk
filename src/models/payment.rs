
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Payment {
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "currency", skip_serializing_if = "Option::is_none")]
    pub currency: Option<String>,
    #[serde(rename = "detail", skip_serializing_if = "Option::is_none")]
    pub detail: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "invoiceRemark", skip_serializing_if = "Option::is_none")]
    pub invoice_remark: Option<String>,
    #[serde(rename = "invoiceTaxId", skip_serializing_if = "Option::is_none")]
    pub invoice_tax_id: Option<String>,
    #[serde(rename = "invoiceTitle", skip_serializing_if = "Option::is_none")]
    pub invoice_title: Option<String>,
    #[serde(rename = "invoiceType", skip_serializing_if = "Option::is_none")]
    pub invoice_type: Option<String>,
    #[serde(rename = "invoiceUrl", skip_serializing_if = "Option::is_none")]
    pub invoice_url: Option<String>,
    #[serde(rename = "message", skip_serializing_if = "Option::is_none")]
    pub message: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "outOrderId", skip_serializing_if = "Option::is_none")]
    pub out_order_id: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "payUrl", skip_serializing_if = "Option::is_none")]
    pub pay_url: Option<String>,
    #[serde(rename = "personEmail", skip_serializing_if = "Option::is_none")]
    pub person_email: Option<String>,
    #[serde(rename = "personIdCard", skip_serializing_if = "Option::is_none")]
    pub person_id_card: Option<String>,
    #[serde(rename = "personName", skip_serializing_if = "Option::is_none")]
    pub person_name: Option<String>,
    #[serde(rename = "personPhone", skip_serializing_if = "Option::is_none")]
    pub person_phone: Option<String>,
    #[serde(rename = "price", skip_serializing_if = "Option::is_none")]
    pub price: Option<f64>,
    #[serde(rename = "productDisplayName", skip_serializing_if = "Option::is_none")]
    pub product_display_name: Option<String>,
    #[serde(rename = "productName", skip_serializing_if = "Option::is_none")]
    pub product_name: Option<String>,
    #[serde(rename = "provider", skip_serializing_if = "Option::is_none")]
    pub provider: Option<String>,
    #[serde(rename = "returnUrl", skip_serializing_if = "Option::is_none")]
    pub return_url: Option<String>,
    #[serde(rename = "state", skip_serializing_if = "Option::is_none")]
    pub state: Option<models::PpPeriodPaymentState>,
    #[serde(rename = "successUrl", skip_serializing_if = "Option::is_none")]
    pub success_url: Option<String>,
    #[serde(rename = "tag", skip_serializing_if = "Option::is_none")]
    pub tag: Option<String>,
    #[serde(rename = "type", skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
    #[serde(rename = "user", skip_serializing_if = "Option::is_none")]
    pub user: Option<String>,
}

impl Payment {
    pub fn new() -> Payment {
        Payment {
            created_time: None,
            currency: None,
            detail: None,
            display_name: None,
            invoice_remark: None,
            invoice_tax_id: None,
            invoice_title: None,
            invoice_type: None,
            invoice_url: None,
            message: None,
            name: None,
            out_order_id: None,
            owner: None,
            pay_url: None,
            person_email: None,
            person_id_card: None,
            person_name: None,
            person_phone: None,
            price: None,
            product_display_name: None,
            product_name: None,
            provider: None,
            return_url: None,
            state: None,
            success_url: None,
            tag: None,
            r#type: None,
            user: None,
        }
    }
}

