
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Syncer {
    #[serde(rename = "affiliationTable", skip_serializing_if = "Option::is_none")]
    pub affiliation_table: Option<String>,
    #[serde(rename = "avatarBaseUrl", skip_serializing_if = "Option::is_none")]
    pub avatar_base_url: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "database", skip_serializing_if = "Option::is_none")]
    pub database: Option<String>,
    #[serde(rename = "databaseType", skip_serializing_if = "Option::is_none")]
    pub database_type: Option<String>,
    #[serde(rename = "errorText", skip_serializing_if = "Option::is_none")]
    pub error_text: Option<String>,
    #[serde(rename = "host", skip_serializing_if = "Option::is_none")]
    pub host: Option<String>,
    #[serde(rename = "isEnabled", skip_serializing_if = "Option::is_none")]
    pub is_enabled: Option<bool>,
    #[serde(rename = "isReadOnly", skip_serializing_if = "Option::is_none")]
    pub is_read_only: Option<bool>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "organization", skip_serializing_if = "Option::is_none")]
    pub organization: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "password", skip_serializing_if = "Option::is_none")]
    pub password: Option<String>,
    #[serde(rename = "port", skip_serializing_if = "Option::is_none")]
    pub port: Option<i64>,
    #[serde(rename = "sslMode", skip_serializing_if = "Option::is_none")]
    pub ssl_mode: Option<String>,
    #[serde(rename = "syncInterval", skip_serializing_if = "Option::is_none")]
    pub sync_interval: Option<i64>,
    #[serde(rename = "table", skip_serializing_if = "Option::is_none")]
    pub table: Option<String>,
    #[serde(rename = "tableColumns", skip_serializing_if = "Option::is_none")]
    pub table_columns: Option<Vec<models::TableColumn>>,
    #[serde(rename = "type", skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
    #[serde(rename = "user", skip_serializing_if = "Option::is_none")]
    pub user: Option<String>,
}

impl Syncer {
    pub fn new() -> Syncer {
        Syncer {
            affiliation_table: None,
            avatar_base_url: None,
            created_time: None,
            database: None,
            database_type: None,
            error_text: None,
            host: None,
            is_enabled: None,
            is_read_only: None,
            name: None,
            organization: None,
            owner: None,
            password: None,
            port: None,
            ssl_mode: None,
            sync_interval: None,
            table: None,
            table_columns: None,
            r#type: None,
            user: None,
        }
    }
}

