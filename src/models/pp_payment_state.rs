
use crate::models;

/// 
#[derive(Clone, Copy, Debug, Eq, PartialEq, Ord, PartialOrd, Hash, Serialize, Deserialize)]
pub enum PpPeriodPaymentState {
    #[serde(rename = "PaymentStatePaid = \"Paid\"")]
    PaymentStatePaidEqualDoubleQuotePaidDoubleQuote,
    #[serde(rename = "PaymentStateCreated = \"Created\"")]
    PaymentStateCreatedEqualDoubleQuoteCreatedDoubleQuote,
    #[serde(rename = "PaymentStateCanceled = \"Canceled\"")]
    PaymentStateCanceledEqualDoubleQuoteCanceledDoubleQuote,
    #[serde(rename = "PaymentStateTimeout = \"Timeout\"")]
    PaymentStateTimeoutEqualDoubleQuoteTimeoutDoubleQuote,
    #[serde(rename = "PaymentStateError = \"Error\"")]
    PaymentStateErrorEqualDoubleQuoteErrorDoubleQuote,

}

impl ToString for PpPeriodPaymentState {
    fn to_string(&self) -> String {
        match self {
            Self::PaymentStatePaidEqualDoubleQuotePaidDoubleQuote => String::from("PaymentStatePaid = \"Paid\""),
            Self::PaymentStateCreatedEqualDoubleQuoteCreatedDoubleQuote => String::from("PaymentStateCreated = \"Created\""),
            Self::PaymentStateCanceledEqualDoubleQuoteCanceledDoubleQuote => String::from("PaymentStateCanceled = \"Canceled\""),
            Self::PaymentStateTimeoutEqualDoubleQuoteTimeoutDoubleQuote => String::from("PaymentStateTimeout = \"Timeout\""),
            Self::PaymentStateErrorEqualDoubleQuoteErrorDoubleQuote => String::from("PaymentStateError = \"Error\""),
        }
    }
}

impl Default for PpPeriodPaymentState {
    fn default() -> PpPeriodPaymentState {
        Self::PaymentStatePaidEqualDoubleQuotePaidDoubleQuote
    }
}

