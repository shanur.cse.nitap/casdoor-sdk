
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Enforcer {
    #[serde(rename = "adapter", skip_serializing_if = "Option::is_none")]
    pub adapter: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "description", skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "model", skip_serializing_if = "Option::is_none")]
    pub model: Option<String>,
    #[serde(rename = "modelCfg", skip_serializing_if = "Option::is_none")]
    pub model_cfg: Option<serde_json::Value>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "updatedTime", skip_serializing_if = "Option::is_none")]
    pub updated_time: Option<String>,
}

impl Enforcer {
    pub fn new() -> Enforcer {
        Enforcer {
            adapter: None,
            created_time: None,
            description: None,
            display_name: None,
            model: None,
            model_cfg: None,
            name: None,
            owner: None,
            updated_time: None,
        }
    }
}

