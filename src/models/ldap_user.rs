
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct LdapUser {
    #[serde(rename = "EmailAddress", skip_serializing_if = "Option::is_none")]
    pub email_address: Option<String>,
    #[serde(rename = "Mail", skip_serializing_if = "Option::is_none")]
    pub mail: Option<String>,
    #[serde(rename = "MobileTelephoneNumber", skip_serializing_if = "Option::is_none")]
    pub mobile_telephone_number: Option<String>,
    #[serde(rename = "PostalAddress", skip_serializing_if = "Option::is_none")]
    pub postal_address: Option<String>,
    #[serde(rename = "RegisteredAddress", skip_serializing_if = "Option::is_none")]
    pub registered_address: Option<String>,
    #[serde(rename = "TelephoneNumber", skip_serializing_if = "Option::is_none")]
    pub telephone_number: Option<String>,
    #[serde(rename = "address", skip_serializing_if = "Option::is_none")]
    pub address: Option<String>,
    #[serde(rename = "cn", skip_serializing_if = "Option::is_none")]
    pub cn: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "email", skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(rename = "gidNumber", skip_serializing_if = "Option::is_none")]
    pub gid_number: Option<String>,
    #[serde(rename = "groupId", skip_serializing_if = "Option::is_none")]
    pub group_id: Option<String>,
    #[serde(rename = "memberOf", skip_serializing_if = "Option::is_none")]
    pub member_of: Option<String>,
    #[serde(rename = "mobile", skip_serializing_if = "Option::is_none")]
    pub mobile: Option<String>,
    #[serde(rename = "uid", skip_serializing_if = "Option::is_none")]
    pub uid: Option<String>,
    #[serde(rename = "uidNumber", skip_serializing_if = "Option::is_none")]
    pub uid_number: Option<String>,
    #[serde(rename = "userPrincipalName", skip_serializing_if = "Option::is_none")]
    pub user_principal_name: Option<String>,
    #[serde(rename = "uuid", skip_serializing_if = "Option::is_none")]
    pub uuid: Option<String>,
}

impl LdapUser {
    pub fn new() -> LdapUser {
        LdapUser {
            email_address: None,
            mail: None,
            mobile_telephone_number: None,
            postal_address: None,
            registered_address: None,
            telephone_number: None,
            address: None,
            cn: None,
            display_name: None,
            email: None,
            gid_number: None,
            group_id: None,
            member_of: None,
            mobile: None,
            uid: None,
            uid_number: None,
            user_principal_name: None,
            uuid: None,
        }
    }
}

