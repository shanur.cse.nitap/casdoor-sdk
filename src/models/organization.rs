
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Organization {
    #[serde(rename = "accountItems", skip_serializing_if = "Option::is_none")]
    pub account_items: Option<Vec<models::AccountItem>>,
    #[serde(rename = "countryCodes", skip_serializing_if = "Option::is_none")]
    pub country_codes: Option<Vec<String>>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "defaultApplication", skip_serializing_if = "Option::is_none")]
    pub default_application: Option<String>,
    #[serde(rename = "defaultAvatar", skip_serializing_if = "Option::is_none")]
    pub default_avatar: Option<String>,
    #[serde(rename = "defaultPassword", skip_serializing_if = "Option::is_none")]
    pub default_password: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "enableSoftDeletion", skip_serializing_if = "Option::is_none")]
    pub enable_soft_deletion: Option<bool>,
    #[serde(rename = "favicon", skip_serializing_if = "Option::is_none")]
    pub favicon: Option<String>,
    #[serde(rename = "initScore", skip_serializing_if = "Option::is_none")]
    pub init_score: Option<i64>,
    #[serde(rename = "isProfilePublic", skip_serializing_if = "Option::is_none")]
    pub is_profile_public: Option<bool>,
    #[serde(rename = "languages", skip_serializing_if = "Option::is_none")]
    pub languages: Option<Vec<String>>,
    #[serde(rename = "masterPassword", skip_serializing_if = "Option::is_none")]
    pub master_password: Option<String>,
    #[serde(rename = "masterVerificationCode", skip_serializing_if = "Option::is_none")]
    pub master_verification_code: Option<String>,
    #[serde(rename = "mfaItems", skip_serializing_if = "Option::is_none")]
    pub mfa_items: Option<Vec<models::MfaItem>>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "passwordOptions", skip_serializing_if = "Option::is_none")]
    pub password_options: Option<Vec<String>>,
    #[serde(rename = "passwordSalt", skip_serializing_if = "Option::is_none")]
    pub password_salt: Option<String>,
    #[serde(rename = "passwordType", skip_serializing_if = "Option::is_none")]
    pub password_type: Option<String>,
    #[serde(rename = "tags", skip_serializing_if = "Option::is_none")]
    pub tags: Option<Vec<String>>,
    #[serde(rename = "themeData", skip_serializing_if = "Option::is_none")]
    pub theme_data: Option<Box<models::ThemeData>>,
    #[serde(rename = "websiteUrl", skip_serializing_if = "Option::is_none")]
    pub website_url: Option<String>,
}

impl Organization {
    pub fn new() -> Organization {
        Organization {
            account_items: None,
            country_codes: None,
            created_time: None,
            default_application: None,
            default_avatar: None,
            default_password: None,
            display_name: None,
            enable_soft_deletion: None,
            favicon: None,
            init_score: None,
            is_profile_public: None,
            languages: None,
            master_password: None,
            master_verification_code: None,
            mfa_items: None,
            name: None,
            owner: None,
            password_options: None,
            password_salt: None,
            password_type: None,
            tags: None,
            theme_data: None,
            website_url: None,
        }
    }
}

