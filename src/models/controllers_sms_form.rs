
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct ControllersSmsForm {
    #[serde(rename = "content", skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,
    #[serde(rename = "organizationId", skip_serializing_if = "Option::is_none")]
    pub organization_id: Option<String>,
    #[serde(rename = "receivers", skip_serializing_if = "Option::is_none")]
    pub receivers: Option<Vec<String>>,
}

impl ControllersSmsForm {
    pub fn new() -> ControllersSmsForm {
        ControllersSmsForm {
            content: None,
            organization_id: None,
            receivers: None,
        }
    }
}

