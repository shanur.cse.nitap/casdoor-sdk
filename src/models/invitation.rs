
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Invitation {
    #[serde(rename = "application", skip_serializing_if = "Option::is_none")]
    pub application: Option<String>,
    #[serde(rename = "code", skip_serializing_if = "Option::is_none")]
    pub code: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "email", skip_serializing_if = "Option::is_none")]
    pub email: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "phone", skip_serializing_if = "Option::is_none")]
    pub phone: Option<String>,
    #[serde(rename = "quota", skip_serializing_if = "Option::is_none")]
    pub quota: Option<i64>,
    #[serde(rename = "signupGroup", skip_serializing_if = "Option::is_none")]
    pub signup_group: Option<String>,
    #[serde(rename = "state", skip_serializing_if = "Option::is_none")]
    pub state: Option<String>,
    #[serde(rename = "updatedTime", skip_serializing_if = "Option::is_none")]
    pub updated_time: Option<String>,
    #[serde(rename = "usedCount", skip_serializing_if = "Option::is_none")]
    pub used_count: Option<i64>,
    #[serde(rename = "username", skip_serializing_if = "Option::is_none")]
    pub username: Option<String>,
}

impl Invitation {
    pub fn new() -> Invitation {
        Invitation {
            application: None,
            code: None,
            created_time: None,
            display_name: None,
            email: None,
            name: None,
            owner: None,
            phone: None,
            quota: None,
            signup_group: None,
            state: None,
            updated_time: None,
            used_count: None,
            username: None,
        }
    }
}

