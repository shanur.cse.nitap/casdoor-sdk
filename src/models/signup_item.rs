
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct SignupItem {
    #[serde(rename = "label", skip_serializing_if = "Option::is_none")]
    pub label: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "placeholder", skip_serializing_if = "Option::is_none")]
    pub placeholder: Option<String>,
    #[serde(rename = "prompted", skip_serializing_if = "Option::is_none")]
    pub prompted: Option<bool>,
    #[serde(rename = "regex", skip_serializing_if = "Option::is_none")]
    pub regex: Option<String>,
    #[serde(rename = "required", skip_serializing_if = "Option::is_none")]
    pub required: Option<bool>,
    #[serde(rename = "rule", skip_serializing_if = "Option::is_none")]
    pub rule: Option<String>,
    #[serde(rename = "visible", skip_serializing_if = "Option::is_none")]
    pub visible: Option<bool>,
}

impl SignupItem {
    pub fn new() -> SignupItem {
        SignupItem {
            label: None,
            name: None,
            placeholder: None,
            prompted: None,
            regex: None,
            required: None,
            rule: None,
            visible: None,
        }
    }
}

