
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct UtilPeriodVersionInfo {
    #[serde(rename = "commitId", skip_serializing_if = "Option::is_none")]
    pub commit_id: Option<String>,
    #[serde(rename = "commitOffset", skip_serializing_if = "Option::is_none")]
    pub commit_offset: Option<i64>,
    #[serde(rename = "version", skip_serializing_if = "Option::is_none")]
    pub version: Option<String>,
}

impl UtilPeriodVersionInfo {
    pub fn new() -> UtilPeriodVersionInfo {
        UtilPeriodVersionInfo {
            commit_id: None,
            commit_offset: None,
            version: None,
        }
    }
}

