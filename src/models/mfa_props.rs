
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct MfaProps {
    #[serde(rename = "countryCode", skip_serializing_if = "Option::is_none")]
    pub country_code: Option<String>,
    #[serde(rename = "enabled", skip_serializing_if = "Option::is_none")]
    pub enabled: Option<bool>,
    #[serde(rename = "isPreferred", skip_serializing_if = "Option::is_none")]
    pub is_preferred: Option<bool>,
    #[serde(rename = "mfaType", skip_serializing_if = "Option::is_none")]
    pub mfa_type: Option<String>,
    #[serde(rename = "recoveryCodes", skip_serializing_if = "Option::is_none")]
    pub recovery_codes: Option<Vec<String>>,
    #[serde(rename = "secret", skip_serializing_if = "Option::is_none")]
    pub secret: Option<String>,
    #[serde(rename = "url", skip_serializing_if = "Option::is_none")]
    pub url: Option<String>,
}

impl MfaProps {
    pub fn new() -> MfaProps {
        MfaProps {
            country_code: None,
            enabled: None,
            is_preferred: None,
            mfa_type: None,
            recovery_codes: None,
            secret: None,
            url: None,
        }
    }
}

