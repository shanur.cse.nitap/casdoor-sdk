
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Permission {
    #[serde(rename = "actions", skip_serializing_if = "Option::is_none")]
    pub actions: Option<Vec<String>>,
    #[serde(rename = "adapter", skip_serializing_if = "Option::is_none")]
    pub adapter: Option<String>,
    #[serde(rename = "approveTime", skip_serializing_if = "Option::is_none")]
    pub approve_time: Option<String>,
    #[serde(rename = "approver", skip_serializing_if = "Option::is_none")]
    pub approver: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "description", skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "domains", skip_serializing_if = "Option::is_none")]
    pub domains: Option<Vec<String>>,
    #[serde(rename = "effect", skip_serializing_if = "Option::is_none")]
    pub effect: Option<String>,
    #[serde(rename = "groups", skip_serializing_if = "Option::is_none")]
    pub groups: Option<Vec<String>>,
    #[serde(rename = "isEnabled", skip_serializing_if = "Option::is_none")]
    pub is_enabled: Option<bool>,
    #[serde(rename = "model", skip_serializing_if = "Option::is_none")]
    pub model: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "resourceType", skip_serializing_if = "Option::is_none")]
    pub resource_type: Option<String>,
    #[serde(rename = "resources", skip_serializing_if = "Option::is_none")]
    pub resources: Option<Vec<String>>,
    #[serde(rename = "roles", skip_serializing_if = "Option::is_none")]
    pub roles: Option<Vec<String>>,
    #[serde(rename = "state", skip_serializing_if = "Option::is_none")]
    pub state: Option<String>,
    #[serde(rename = "submitter", skip_serializing_if = "Option::is_none")]
    pub submitter: Option<String>,
    #[serde(rename = "users", skip_serializing_if = "Option::is_none")]
    pub users: Option<Vec<String>>,
}

impl Permission {
    pub fn new() -> Permission {
        Permission {
            actions: None,
            adapter: None,
            approve_time: None,
            approver: None,
            created_time: None,
            description: None,
            display_name: None,
            domains: None,
            effect: None,
            groups: None,
            is_enabled: None,
            model: None,
            name: None,
            owner: None,
            resource_type: None,
            resources: None,
            roles: None,
            state: None,
            submitter: None,
            users: None,
        }
    }
}

