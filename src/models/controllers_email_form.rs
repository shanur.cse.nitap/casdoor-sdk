
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct ControllersEmailForm {
    #[serde(rename = "content", skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,
    #[serde(rename = "provider", skip_serializing_if = "Option::is_none")]
    pub provider: Option<String>,
    #[serde(rename = "receivers", skip_serializing_if = "Option::is_none")]
    pub receivers: Option<Vec<String>>,
    #[serde(rename = "sender", skip_serializing_if = "Option::is_none")]
    pub sender: Option<String>,
    #[serde(rename = "title", skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
}

impl ControllersEmailForm {
    pub fn new() -> ControllersEmailForm {
        ControllersEmailForm {
            content: None,
            provider: None,
            receivers: None,
            sender: None,
            title: None,
        }
    }
}

