
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct TableColumn {
    #[serde(rename = "casdoorName", skip_serializing_if = "Option::is_none")]
    pub casdoor_name: Option<String>,
    #[serde(rename = "isHashed", skip_serializing_if = "Option::is_none")]
    pub is_hashed: Option<bool>,
    #[serde(rename = "isKey", skip_serializing_if = "Option::is_none")]
    pub is_key: Option<bool>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "type", skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
    #[serde(rename = "values", skip_serializing_if = "Option::is_none")]
    pub values: Option<Vec<String>>,
}

impl TableColumn {
    pub fn new() -> TableColumn {
        TableColumn {
            casdoor_name: None,
            is_hashed: None,
            is_key: None,
            name: None,
            r#type: None,
            values: None,
        }
    }
}

