
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct MfaItem {
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "rule", skip_serializing_if = "Option::is_none")]
    pub rule: Option<String>,
}

impl MfaItem {
    pub fn new() -> MfaItem {
        MfaItem {
            name: None,
            rule: None,
        }
    }
}

