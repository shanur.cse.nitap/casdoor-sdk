
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct GaugeVecInfo {
    #[serde(rename = "method", skip_serializing_if = "Option::is_none")]
    pub method: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "throughput", skip_serializing_if = "Option::is_none")]
    pub throughput: Option<f64>,
}

impl GaugeVecInfo {
    pub fn new() -> GaugeVecInfo {
        GaugeVecInfo {
            method: None,
            name: None,
            throughput: None,
        }
    }
}

