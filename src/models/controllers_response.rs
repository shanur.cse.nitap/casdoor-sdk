
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct ControllersResponse {
    #[serde(rename = "data", skip_serializing_if = "Option::is_none")]
    pub data: Option<serde_json::Value>,
    #[serde(rename = "data2", skip_serializing_if = "Option::is_none")]
    pub data2: Option<serde_json::Value>,
    #[serde(rename = "msg", skip_serializing_if = "Option::is_none")]
    pub msg: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "status", skip_serializing_if = "Option::is_none")]
    pub status: Option<String>,
    #[serde(rename = "sub", skip_serializing_if = "Option::is_none")]
    pub sub: Option<String>,
}

impl ControllersResponse {
    pub fn new() -> ControllersResponse {
        ControllersResponse {
            data: None,
            data2: None,
            msg: None,
            name: None,
            status: None,
            sub: None,
        }
    }
}

