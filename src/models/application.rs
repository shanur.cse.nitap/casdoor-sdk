
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Application {
    #[serde(rename = "affiliationUrl", skip_serializing_if = "Option::is_none")]
    pub affiliation_url: Option<String>,
    #[serde(rename = "cert", skip_serializing_if = "Option::is_none")]
    pub cert: Option<String>,
    #[serde(rename = "certPublicKey", skip_serializing_if = "Option::is_none")]
    pub cert_public_key: Option<String>,
    #[serde(rename = "clientId", skip_serializing_if = "Option::is_none")]
    pub client_id: Option<String>,
    #[serde(rename = "clientSecret", skip_serializing_if = "Option::is_none")]
    pub client_secret: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "description", skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "enableAutoSignin", skip_serializing_if = "Option::is_none")]
    pub enable_auto_signin: Option<bool>,
    #[serde(rename = "enableCodeSignin", skip_serializing_if = "Option::is_none")]
    pub enable_code_signin: Option<bool>,
    #[serde(rename = "enableLinkWithEmail", skip_serializing_if = "Option::is_none")]
    pub enable_link_with_email: Option<bool>,
    #[serde(rename = "enablePassword", skip_serializing_if = "Option::is_none")]
    pub enable_password: Option<bool>,
    #[serde(rename = "enableSamlC14n10", skip_serializing_if = "Option::is_none")]
    pub enable_saml_c14n10: Option<bool>,
    #[serde(rename = "enableSamlCompress", skip_serializing_if = "Option::is_none")]
    pub enable_saml_compress: Option<bool>,
    #[serde(rename = "enableSamlPostBinding", skip_serializing_if = "Option::is_none")]
    pub enable_saml_post_binding: Option<bool>,
    #[serde(rename = "enableSignUp", skip_serializing_if = "Option::is_none")]
    pub enable_sign_up: Option<bool>,
    #[serde(rename = "enableSigninSession", skip_serializing_if = "Option::is_none")]
    pub enable_signin_session: Option<bool>,
    #[serde(rename = "enableWebAuthn", skip_serializing_if = "Option::is_none")]
    pub enable_web_authn: Option<bool>,
    #[serde(rename = "expireInHours", skip_serializing_if = "Option::is_none")]
    pub expire_in_hours: Option<i64>,
    #[serde(rename = "failedSigninFrozenTime", skip_serializing_if = "Option::is_none")]
    pub failed_signin_frozen_time: Option<i64>,
    #[serde(rename = "failedSigninLimit", skip_serializing_if = "Option::is_none")]
    pub failed_signin_limit: Option<i64>,
    #[serde(rename = "forgetUrl", skip_serializing_if = "Option::is_none")]
    pub forget_url: Option<String>,
    #[serde(rename = "formBackgroundUrl", skip_serializing_if = "Option::is_none")]
    pub form_background_url: Option<String>,
    #[serde(rename = "formCss", skip_serializing_if = "Option::is_none")]
    pub form_css: Option<String>,
    #[serde(rename = "formCssMobile", skip_serializing_if = "Option::is_none")]
    pub form_css_mobile: Option<String>,
    #[serde(rename = "formOffset", skip_serializing_if = "Option::is_none")]
    pub form_offset: Option<i64>,
    #[serde(rename = "formSideHtml", skip_serializing_if = "Option::is_none")]
    pub form_side_html: Option<String>,
    #[serde(rename = "grantTypes", skip_serializing_if = "Option::is_none")]
    pub grant_types: Option<Vec<String>>,
    #[serde(rename = "homepageUrl", skip_serializing_if = "Option::is_none")]
    pub homepage_url: Option<String>,
    #[serde(rename = "invitationCodes", skip_serializing_if = "Option::is_none")]
    pub invitation_codes: Option<Vec<String>>,
    #[serde(rename = "logo", skip_serializing_if = "Option::is_none")]
    pub logo: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "orgChoiceMode", skip_serializing_if = "Option::is_none")]
    pub org_choice_mode: Option<String>,
    #[serde(rename = "organization", skip_serializing_if = "Option::is_none")]
    pub organization: Option<String>,
    #[serde(rename = "organizationObj", skip_serializing_if = "Option::is_none")]
    pub organization_obj: Option<Box<models::Organization>>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "providers", skip_serializing_if = "Option::is_none")]
    pub providers: Option<Vec<models::ProviderItem>>,
    #[serde(rename = "redirectUris", skip_serializing_if = "Option::is_none")]
    pub redirect_uris: Option<Vec<String>>,
    #[serde(rename = "refreshExpireInHours", skip_serializing_if = "Option::is_none")]
    pub refresh_expire_in_hours: Option<i64>,
    #[serde(rename = "samlAttributes", skip_serializing_if = "Option::is_none")]
    pub saml_attributes: Option<Vec<models::SamlItem>>,
    #[serde(rename = "samlReplyUrl", skip_serializing_if = "Option::is_none")]
    pub saml_reply_url: Option<String>,
    #[serde(rename = "signinHtml", skip_serializing_if = "Option::is_none")]
    pub signin_html: Option<String>,
    #[serde(rename = "signinMethods", skip_serializing_if = "Option::is_none")]
    pub signin_methods: Option<Vec<models::SigninMethod>>,
    #[serde(rename = "signinUrl", skip_serializing_if = "Option::is_none")]
    pub signin_url: Option<String>,
    #[serde(rename = "signupHtml", skip_serializing_if = "Option::is_none")]
    pub signup_html: Option<String>,
    #[serde(rename = "signupItems", skip_serializing_if = "Option::is_none")]
    pub signup_items: Option<Vec<models::SignupItem>>,
    #[serde(rename = "signupUrl", skip_serializing_if = "Option::is_none")]
    pub signup_url: Option<String>,
    #[serde(rename = "tags", skip_serializing_if = "Option::is_none")]
    pub tags: Option<Vec<String>>,
    #[serde(rename = "termsOfUse", skip_serializing_if = "Option::is_none")]
    pub terms_of_use: Option<String>,
    #[serde(rename = "themeData", skip_serializing_if = "Option::is_none")]
    pub theme_data: Option<Box<models::ThemeData>>,
    #[serde(rename = "tokenFields", skip_serializing_if = "Option::is_none")]
    pub token_fields: Option<Vec<String>>,
    #[serde(rename = "tokenFormat", skip_serializing_if = "Option::is_none")]
    pub token_format: Option<String>,
}

impl Application {
    pub fn new() -> Application {
        Application {
            affiliation_url: None,
            cert: None,
            cert_public_key: None,
            client_id: None,
            client_secret: None,
            created_time: None,
            description: None,
            display_name: None,
            enable_auto_signin: None,
            enable_code_signin: None,
            enable_link_with_email: None,
            enable_password: None,
            enable_saml_c14n10: None,
            enable_saml_compress: None,
            enable_saml_post_binding: None,
            enable_sign_up: None,
            enable_signin_session: None,
            enable_web_authn: None,
            expire_in_hours: None,
            failed_signin_frozen_time: None,
            failed_signin_limit: None,
            forget_url: None,
            form_background_url: None,
            form_css: None,
            form_css_mobile: None,
            form_offset: None,
            form_side_html: None,
            grant_types: None,
            homepage_url: None,
            invitation_codes: None,
            logo: None,
            name: None,
            org_choice_mode: None,
            organization: None,
            organization_obj: None,
            owner: None,
            providers: None,
            redirect_uris: None,
            refresh_expire_in_hours: None,
            saml_attributes: None,
            saml_reply_url: None,
            signin_html: None,
            signin_methods: None,
            signin_url: None,
            signup_html: None,
            signup_items: None,
            signup_url: None,
            tags: None,
            terms_of_use: None,
            theme_data: None,
            token_fields: None,
            token_format: None,
        }
    }
}

