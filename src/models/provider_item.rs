
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct ProviderItem {
    #[serde(rename = "canSignIn", skip_serializing_if = "Option::is_none")]
    pub can_sign_in: Option<bool>,
    #[serde(rename = "canSignUp", skip_serializing_if = "Option::is_none")]
    pub can_sign_up: Option<bool>,
    #[serde(rename = "canUnlink", skip_serializing_if = "Option::is_none")]
    pub can_unlink: Option<bool>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "prompted", skip_serializing_if = "Option::is_none")]
    pub prompted: Option<bool>,
    #[serde(rename = "provider", skip_serializing_if = "Option::is_none")]
    pub provider: Option<Box<models::Provider>>,
    #[serde(rename = "rule", skip_serializing_if = "Option::is_none")]
    pub rule: Option<String>,
    #[serde(rename = "signupGroup", skip_serializing_if = "Option::is_none")]
    pub signup_group: Option<String>,
}

impl ProviderItem {
    pub fn new() -> ProviderItem {
        ProviderItem {
            can_sign_in: None,
            can_sign_up: None,
            can_unlink: None,
            name: None,
            owner: None,
            prompted: None,
            provider: None,
            rule: None,
            signup_group: None,
        }
    }
}

