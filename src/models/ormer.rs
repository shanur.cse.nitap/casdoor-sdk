
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Ormer {
    #[serde(rename = "Engine", skip_serializing_if = "Option::is_none")]
    pub engine: Option<serde_json::Value>,
    #[serde(rename = "dataSourceName", skip_serializing_if = "Option::is_none")]
    pub data_source_name: Option<String>,
    #[serde(rename = "dbName", skip_serializing_if = "Option::is_none")]
    pub db_name: Option<String>,
    #[serde(rename = "driverName", skip_serializing_if = "Option::is_none")]
    pub driver_name: Option<String>,
}

impl Ormer {
    pub fn new() -> Ormer {
        Ormer {
            engine: None,
            data_source_name: None,
            db_name: None,
            driver_name: None,
        }
    }
}

