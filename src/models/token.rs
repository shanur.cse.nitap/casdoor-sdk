
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Token {
    #[serde(rename = "accessToken", skip_serializing_if = "Option::is_none")]
    pub access_token: Option<String>,
    #[serde(rename = "accessTokenHash", skip_serializing_if = "Option::is_none")]
    pub access_token_hash: Option<String>,
    #[serde(rename = "application", skip_serializing_if = "Option::is_none")]
    pub application: Option<String>,
    #[serde(rename = "code", skip_serializing_if = "Option::is_none")]
    pub code: Option<String>,
    #[serde(rename = "codeChallenge", skip_serializing_if = "Option::is_none")]
    pub code_challenge: Option<String>,
    #[serde(rename = "codeExpireIn", skip_serializing_if = "Option::is_none")]
    pub code_expire_in: Option<i64>,
    #[serde(rename = "codeIsUsed", skip_serializing_if = "Option::is_none")]
    pub code_is_used: Option<bool>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "expiresIn", skip_serializing_if = "Option::is_none")]
    pub expires_in: Option<i64>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "organization", skip_serializing_if = "Option::is_none")]
    pub organization: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "refreshToken", skip_serializing_if = "Option::is_none")]
    pub refresh_token: Option<String>,
    #[serde(rename = "refreshTokenHash", skip_serializing_if = "Option::is_none")]
    pub refresh_token_hash: Option<String>,
    #[serde(rename = "scope", skip_serializing_if = "Option::is_none")]
    pub scope: Option<String>,
    #[serde(rename = "tokenType", skip_serializing_if = "Option::is_none")]
    pub token_type: Option<String>,
    #[serde(rename = "user", skip_serializing_if = "Option::is_none")]
    pub user: Option<String>,
}

impl Token {
    pub fn new() -> Token {
        Token {
            access_token: None,
            access_token_hash: None,
            application: None,
            code: None,
            code_challenge: None,
            code_expire_in: None,
            code_is_used: None,
            created_time: None,
            expires_in: None,
            name: None,
            organization: None,
            owner: None,
            refresh_token: None,
            refresh_token_hash: None,
            scope: None,
            token_type: None,
            user: None,
        }
    }
}

