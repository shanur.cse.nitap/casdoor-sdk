
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct HistogramVecInfo {
    #[serde(rename = "count", skip_serializing_if = "Option::is_none")]
    pub count: Option<i64>,
    #[serde(rename = "latency", skip_serializing_if = "Option::is_none")]
    pub latency: Option<String>,
    #[serde(rename = "method", skip_serializing_if = "Option::is_none")]
    pub method: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
}

impl HistogramVecInfo {
    pub fn new() -> HistogramVecInfo {
        HistogramVecInfo {
            count: None,
            latency: None,
            method: None,
            name: None,
        }
    }
}

