
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct UtilPeriodSystemInfo {
    #[serde(rename = "cpuUsage", skip_serializing_if = "Option::is_none")]
    pub cpu_usage: Option<Vec<f64>>,
    #[serde(rename = "memoryTotal", skip_serializing_if = "Option::is_none")]
    pub memory_total: Option<i64>,
    #[serde(rename = "memoryUsed", skip_serializing_if = "Option::is_none")]
    pub memory_used: Option<i64>,
}

impl UtilPeriodSystemInfo {
    pub fn new() -> UtilPeriodSystemInfo {
        UtilPeriodSystemInfo {
            cpu_usage: None,
            memory_total: None,
            memory_used: None,
        }
    }
}

