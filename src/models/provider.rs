
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Provider {
    #[serde(rename = "appId", skip_serializing_if = "Option::is_none")]
    pub app_id: Option<String>,
    #[serde(rename = "bucket", skip_serializing_if = "Option::is_none")]
    pub bucket: Option<String>,
    #[serde(rename = "category", skip_serializing_if = "Option::is_none")]
    pub category: Option<String>,
    #[serde(rename = "cert", skip_serializing_if = "Option::is_none")]
    pub cert: Option<String>,
    #[serde(rename = "clientId", skip_serializing_if = "Option::is_none")]
    pub client_id: Option<String>,
    #[serde(rename = "clientId2", skip_serializing_if = "Option::is_none")]
    pub client_id2: Option<String>,
    #[serde(rename = "clientSecret", skip_serializing_if = "Option::is_none")]
    pub client_secret: Option<String>,
    #[serde(rename = "clientSecret2", skip_serializing_if = "Option::is_none")]
    pub client_secret2: Option<String>,
    #[serde(rename = "content", skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "customAuthUrl", skip_serializing_if = "Option::is_none")]
    pub custom_auth_url: Option<String>,
    #[serde(rename = "customLogo", skip_serializing_if = "Option::is_none")]
    pub custom_logo: Option<String>,
    #[serde(rename = "customTokenUrl", skip_serializing_if = "Option::is_none")]
    pub custom_token_url: Option<String>,
    #[serde(rename = "customUserInfoUrl", skip_serializing_if = "Option::is_none")]
    pub custom_user_info_url: Option<String>,
    #[serde(rename = "disableSsl", skip_serializing_if = "Option::is_none")]
    pub disable_ssl: Option<bool>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "domain", skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,
    #[serde(rename = "enableSignAuthnRequest", skip_serializing_if = "Option::is_none")]
    pub enable_sign_authn_request: Option<bool>,
    #[serde(rename = "endpoint", skip_serializing_if = "Option::is_none")]
    pub endpoint: Option<String>,
    #[serde(rename = "host", skip_serializing_if = "Option::is_none")]
    pub host: Option<String>,
    #[serde(rename = "idP", skip_serializing_if = "Option::is_none")]
    pub id_p: Option<String>,
    #[serde(rename = "intranetEndpoint", skip_serializing_if = "Option::is_none")]
    pub intranet_endpoint: Option<String>,
    #[serde(rename = "issuerUrl", skip_serializing_if = "Option::is_none")]
    pub issuer_url: Option<String>,
    #[serde(rename = "metadata", skip_serializing_if = "Option::is_none")]
    pub metadata: Option<String>,
    #[serde(rename = "method", skip_serializing_if = "Option::is_none")]
    pub method: Option<String>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "pathPrefix", skip_serializing_if = "Option::is_none")]
    pub path_prefix: Option<String>,
    #[serde(rename = "port", skip_serializing_if = "Option::is_none")]
    pub port: Option<i64>,
    #[serde(rename = "providerUrl", skip_serializing_if = "Option::is_none")]
    pub provider_url: Option<String>,
    #[serde(rename = "receiver", skip_serializing_if = "Option::is_none")]
    pub receiver: Option<String>,
    #[serde(rename = "regionId", skip_serializing_if = "Option::is_none")]
    pub region_id: Option<String>,
    #[serde(rename = "scopes", skip_serializing_if = "Option::is_none")]
    pub scopes: Option<String>,
    #[serde(rename = "signName", skip_serializing_if = "Option::is_none")]
    pub sign_name: Option<String>,
    #[serde(rename = "subType", skip_serializing_if = "Option::is_none")]
    pub sub_type: Option<String>,
    #[serde(rename = "templateCode", skip_serializing_if = "Option::is_none")]
    pub template_code: Option<String>,
    #[serde(rename = "title", skip_serializing_if = "Option::is_none")]
    pub title: Option<String>,
    #[serde(rename = "type", skip_serializing_if = "Option::is_none")]
    pub r#type: Option<String>,
    #[serde(rename = "userMapping", skip_serializing_if = "Option::is_none")]
    pub user_mapping: Option<serde_json::Value>,
}

impl Provider {
    pub fn new() -> Provider {
        Provider {
            app_id: None,
            bucket: None,
            category: None,
            cert: None,
            client_id: None,
            client_id2: None,
            client_secret: None,
            client_secret2: None,
            content: None,
            created_time: None,
            custom_auth_url: None,
            custom_logo: None,
            custom_token_url: None,
            custom_user_info_url: None,
            disable_ssl: None,
            display_name: None,
            domain: None,
            enable_sign_authn_request: None,
            endpoint: None,
            host: None,
            id_p: None,
            intranet_endpoint: None,
            issuer_url: None,
            metadata: None,
            method: None,
            name: None,
            owner: None,
            path_prefix: None,
            port: None,
            provider_url: None,
            receiver: None,
            region_id: None,
            scopes: None,
            sign_name: None,
            sub_type: None,
            template_code: None,
            title: None,
            r#type: None,
            user_mapping: None,
        }
    }
}

