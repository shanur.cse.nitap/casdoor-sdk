
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Ldap {
    #[serde(rename = "autoSync", skip_serializing_if = "Option::is_none")]
    pub auto_sync: Option<i64>,
    #[serde(rename = "baseDn", skip_serializing_if = "Option::is_none")]
    pub base_dn: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "enableSsl", skip_serializing_if = "Option::is_none")]
    pub enable_ssl: Option<bool>,
    #[serde(rename = "filter", skip_serializing_if = "Option::is_none")]
    pub filter: Option<String>,
    #[serde(rename = "filterFields", skip_serializing_if = "Option::is_none")]
    pub filter_fields: Option<Vec<String>>,
    #[serde(rename = "host", skip_serializing_if = "Option::is_none")]
    pub host: Option<String>,
    #[serde(rename = "id", skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "lastSync", skip_serializing_if = "Option::is_none")]
    pub last_sync: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "password", skip_serializing_if = "Option::is_none")]
    pub password: Option<String>,
    #[serde(rename = "port", skip_serializing_if = "Option::is_none")]
    pub port: Option<i64>,
    #[serde(rename = "serverName", skip_serializing_if = "Option::is_none")]
    pub server_name: Option<String>,
    #[serde(rename = "username", skip_serializing_if = "Option::is_none")]
    pub username: Option<String>,
}

impl Ldap {
    pub fn new() -> Ldap {
        Ldap {
            auto_sync: None,
            base_dn: None,
            created_time: None,
            enable_ssl: None,
            filter: None,
            filter_fields: None,
            host: None,
            id: None,
            last_sync: None,
            owner: None,
            password: None,
            port: None,
            server_name: None,
            username: None,
        }
    }
}

