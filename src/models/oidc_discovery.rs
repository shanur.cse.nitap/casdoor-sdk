
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct OidcDiscovery {
    #[serde(rename = "authorization_endpoint", skip_serializing_if = "Option::is_none")]
    pub authorization_endpoint: Option<String>,
    #[serde(rename = "claims_supported", skip_serializing_if = "Option::is_none")]
    pub claims_supported: Option<Vec<String>>,
    #[serde(rename = "end_session_endpoint", skip_serializing_if = "Option::is_none")]
    pub end_session_endpoint: Option<String>,
    #[serde(rename = "grant_types_supported", skip_serializing_if = "Option::is_none")]
    pub grant_types_supported: Option<Vec<String>>,
    #[serde(rename = "id_token_signing_alg_values_supported", skip_serializing_if = "Option::is_none")]
    pub id_token_signing_alg_values_supported: Option<Vec<String>>,
    #[serde(rename = "introspection_endpoint", skip_serializing_if = "Option::is_none")]
    pub introspection_endpoint: Option<String>,
    #[serde(rename = "issuer", skip_serializing_if = "Option::is_none")]
    pub issuer: Option<String>,
    #[serde(rename = "jwks_uri", skip_serializing_if = "Option::is_none")]
    pub jwks_uri: Option<String>,
    #[serde(rename = "request_object_signing_alg_values_supported", skip_serializing_if = "Option::is_none")]
    pub request_object_signing_alg_values_supported: Option<Vec<String>>,
    #[serde(rename = "request_parameter_supported", skip_serializing_if = "Option::is_none")]
    pub request_parameter_supported: Option<bool>,
    #[serde(rename = "response_modes_supported", skip_serializing_if = "Option::is_none")]
    pub response_modes_supported: Option<Vec<String>>,
    #[serde(rename = "response_types_supported", skip_serializing_if = "Option::is_none")]
    pub response_types_supported: Option<Vec<String>>,
    #[serde(rename = "scopes_supported", skip_serializing_if = "Option::is_none")]
    pub scopes_supported: Option<Vec<String>>,
    #[serde(rename = "subject_types_supported", skip_serializing_if = "Option::is_none")]
    pub subject_types_supported: Option<Vec<String>>,
    #[serde(rename = "token_endpoint", skip_serializing_if = "Option::is_none")]
    pub token_endpoint: Option<String>,
    #[serde(rename = "userinfo_endpoint", skip_serializing_if = "Option::is_none")]
    pub userinfo_endpoint: Option<String>,
}

impl OidcDiscovery {
    pub fn new() -> OidcDiscovery {
        OidcDiscovery {
            authorization_endpoint: None,
            claims_supported: None,
            end_session_endpoint: None,
            grant_types_supported: None,
            id_token_signing_alg_values_supported: None,
            introspection_endpoint: None,
            issuer: None,
            jwks_uri: None,
            request_object_signing_alg_values_supported: None,
            request_parameter_supported: None,
            response_modes_supported: None,
            response_types_supported: None,
            scopes_supported: None,
            subject_types_supported: None,
            token_endpoint: None,
            userinfo_endpoint: None,
        }
    }
}

