
use crate::models;

#[derive(Clone, Default, Debug, PartialEq, Serialize, Deserialize)]
pub struct Pricing {
    #[serde(rename = "application", skip_serializing_if = "Option::is_none")]
    pub application: Option<String>,
    #[serde(rename = "createdTime", skip_serializing_if = "Option::is_none")]
    pub created_time: Option<String>,
    #[serde(rename = "description", skip_serializing_if = "Option::is_none")]
    pub description: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "isEnabled", skip_serializing_if = "Option::is_none")]
    pub is_enabled: Option<bool>,
    #[serde(rename = "name", skip_serializing_if = "Option::is_none")]
    pub name: Option<String>,
    #[serde(rename = "owner", skip_serializing_if = "Option::is_none")]
    pub owner: Option<String>,
    #[serde(rename = "plans", skip_serializing_if = "Option::is_none")]
    pub plans: Option<Vec<String>>,
    #[serde(rename = "trialDuration", skip_serializing_if = "Option::is_none")]
    pub trial_duration: Option<i64>,
}

impl Pricing {
    pub fn new() -> Pricing {
        Pricing {
            application: None,
            created_time: None,
            description: None,
            display_name: None,
            is_enabled: None,
            name: None,
            owner: None,
            plans: None,
            trial_duration: None,
        }
    }
}

