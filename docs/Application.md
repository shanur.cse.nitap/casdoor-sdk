# Application

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**affiliation_url** | Option<**String**> |  | [optional]
**cert** | Option<**String**> |  | [optional]
**cert_public_key** | Option<**String**> |  | [optional]
**client_id** | Option<**String**> |  | [optional]
**client_secret** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**enable_auto_signin** | Option<**bool**> |  | [optional]
**enable_code_signin** | Option<**bool**> |  | [optional]
**enable_link_with_email** | Option<**bool**> |  | [optional]
**enable_password** | Option<**bool**> |  | [optional]
**enable_saml_c14n10** | Option<**bool**> |  | [optional]
**enable_saml_compress** | Option<**bool**> |  | [optional]
**enable_saml_post_binding** | Option<**bool**> |  | [optional]
**enable_sign_up** | Option<**bool**> |  | [optional]
**enable_signin_session** | Option<**bool**> |  | [optional]
**enable_web_authn** | Option<**bool**> |  | [optional]
**expire_in_hours** | Option<**i64**> |  | [optional]
**failed_signin_frozen_time** | Option<**i64**> |  | [optional]
**failed_signin_limit** | Option<**i64**> |  | [optional]
**forget_url** | Option<**String**> |  | [optional]
**form_background_url** | Option<**String**> |  | [optional]
**form_css** | Option<**String**> |  | [optional]
**form_css_mobile** | Option<**String**> |  | [optional]
**form_offset** | Option<**i64**> |  | [optional]
**form_side_html** | Option<**String**> |  | [optional]
**grant_types** | Option<**Vec<String>**> |  | [optional]
**homepage_url** | Option<**String**> |  | [optional]
**invitation_codes** | Option<**Vec<String>**> |  | [optional]
**logo** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**org_choice_mode** | Option<**String**> |  | [optional]
**organization** | Option<**String**> |  | [optional]
**organization_obj** | Option<[**models::Organization**](object.Organization.md)> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**providers** | Option<[**Vec<models::ProviderItem>**](object.ProviderItem.md)> |  | [optional]
**redirect_uris** | Option<**Vec<String>**> |  | [optional]
**refresh_expire_in_hours** | Option<**i64**> |  | [optional]
**saml_attributes** | Option<[**Vec<models::SamlItem>**](object.SamlItem.md)> |  | [optional]
**saml_reply_url** | Option<**String**> |  | [optional]
**signin_html** | Option<**String**> |  | [optional]
**signin_methods** | Option<[**Vec<models::SigninMethod>**](object.SigninMethod.md)> |  | [optional]
**signin_url** | Option<**String**> |  | [optional]
**signup_html** | Option<**String**> |  | [optional]
**signup_items** | Option<[**Vec<models::SignupItem>**](object.SignupItem.md)> |  | [optional]
**signup_url** | Option<**String**> |  | [optional]
**tags** | Option<**Vec<String>**> |  | [optional]
**terms_of_use** | Option<**String**> |  | [optional]
**theme_data** | Option<[**models::ThemeData**](object.ThemeData.md)> |  | [optional]
**token_fields** | Option<**Vec<String>**> |  | [optional]
**token_format** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


