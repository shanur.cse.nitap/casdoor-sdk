# casdoor\AccountApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_ldap**](AccountApi.md#add_ldap) | **POST** /api/add-ldap | 
[**delete_ldap**](AccountApi.md#delete_ldap) | **POST** /api/delete-ldap | 
[**get_account**](AccountApi.md#get_account) | **GET** /api/get-account | 
[**get_ldap**](AccountApi.md#get_ldap) | **GET** /api/get-ldap | 
[**get_ldaps**](AccountApi.md#get_ldaps) | **GET** /api/get-ldaps | 
[**get_ldapser**](AccountApi.md#get_ldapser) | **GET** /api/get-ldap-users | 
[**reset_email_or_phone**](AccountApi.md#reset_email_or_phone) | **POST** /api/reset-email-or-phone | 
[**set_password**](AccountApi.md#set_password) | **POST** /api/set-password | 
[**sync_ldap_users**](AccountApi.md#sync_ldap_users) | **POST** /api/sync-ldap-users | 
[**update_ldap**](AccountApi.md#update_ldap) | **POST** /api/update-ldap | 
[**user_info**](AccountApi.md#user_info) | **GET** /api/userinfo | 
[**user_info2**](AccountApi.md#user_info2) | **GET** /api/user | 



## add_ldap

> models::ControllersResponse add_ldap(body)


add ldap

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Ldap**](Ldap.md) | The details of the ldap | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_ldap

> models::ControllersResponse delete_ldap(body)


delete ldap

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Ldap**](Ldap.md) | The details of the ldap | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_account

> models::ControllersResponse get_account()


get the details of the current account

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_ldap

> models::Ldap get_ldap(id)


get ldap

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | id | [required] |

### Return type

[**models::Ldap**](object.Ldap.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_ldaps

> Vec<models::Ldap> get_ldaps(owner)


get ldaps

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | Option<**String**> | owner |  |

### Return type

[**Vec<models::Ldap>**](object.Ldap.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_ldapser

> models::ControllersLdapResp get_ldapser()


get ldap users

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::ControllersLdapResp**](controllers.LdapResp.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## reset_email_or_phone

> models::Userinfo reset_email_or_phone()


### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## set_password

> models::ControllersResponse set_password(user_owner, user_name, old_password, new_password)


set password

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**user_owner** | **String** | The owner of the user | [required] |
**user_name** | **String** | The name of the user | [required] |
**old_password** | **String** | The old password of the user | [required] |
**new_password** | **String** | The new password of the user | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## sync_ldap_users

> models::ControllersLdapSyncResp sync_ldap_users(id)


sync ldap users

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | id | [required] |

### Return type

[**models::ControllersLdapSyncResp**](controllers.LdapSyncResp.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_ldap

> models::ControllersResponse update_ldap(body)


update ldap

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Ldap**](Ldap.md) | The details of the ldap | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## user_info

> models::Userinfo user_info()


return user information according to OIDC standards

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## user_info2

> models::ControllersLaravelResponse user_info2()


return Laravel compatible user information according to OAuth 2.0

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::ControllersLaravelResponse**](controllers.LaravelResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

