# casdoor\PermissionApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_permission**](PermissionApi.md#add_permission) | **POST** /api/add-permission | 
[**delete_permission**](PermissionApi.md#delete_permission) | **POST** /api/delete-permission | 
[**get_permission**](PermissionApi.md#get_permission) | **GET** /api/get-permission | 
[**get_permissions**](PermissionApi.md#get_permissions) | **GET** /api/get-permissions | 
[**get_permissions_by_role**](PermissionApi.md#get_permissions_by_role) | **GET** /api/get-permissions-by-role | 
[**get_permissions_by_submitter**](PermissionApi.md#get_permissions_by_submitter) | **GET** /api/get-permissions-by-submitter | 
[**update_permission**](PermissionApi.md#update_permission) | **POST** /api/update-permission | 



## add_permission

> models::ControllersResponse add_permission(body)


add permission

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Permission**](Permission.md) | The details of the permission | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_permission

> models::ControllersResponse delete_permission(body)


delete permission

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Permission**](Permission.md) | The details of the permission | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_permission

> models::Permission get_permission(id)


get permission

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the permission | [required] |

### Return type

[**models::Permission**](object.Permission.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_permissions

> Vec<models::Permission> get_permissions(owner)


get permissions

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of permissions | [required] |

### Return type

[**Vec<models::Permission>**](object.Permission.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_permissions_by_role

> Vec<models::Permission> get_permissions_by_role(id)


get permissions by role

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the role | [required] |

### Return type

[**Vec<models::Permission>**](object.Permission.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_permissions_by_submitter

> Vec<models::Permission> get_permissions_by_submitter()


get permissions by submitter

### Parameters

This endpoint does not need any parameter.

### Return type

[**Vec<models::Permission>**](object.Permission.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_permission

> models::ControllersResponse update_permission(id, body)


update permission

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the permission | [required] |
**body** | [**Permission**](Permission.md) | The details of the permission | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

