# TableColumn

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**casdoor_name** | Option<**String**> |  | [optional]
**is_hashed** | Option<**bool**> |  | [optional]
**is_key** | Option<**bool**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**r#type** | Option<**String**> |  | [optional]
**values** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


