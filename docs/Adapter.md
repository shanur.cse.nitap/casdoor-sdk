# Adapter

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_time** | Option<**String**> |  | [optional]
**database** | Option<**String**> |  | [optional]
**database_type** | Option<**String**> |  | [optional]
**host** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**password** | Option<**String**> |  | [optional]
**port** | Option<**i64**> |  | [optional]
**table** | Option<**String**> |  | [optional]
**r#type** | Option<**String**> |  | [optional]
**use_same_db** | Option<**bool**> |  | [optional]
**user** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


