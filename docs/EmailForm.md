# ControllersEmailForm

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content** | Option<**String**> |  | [optional]
**provider** | Option<**String**> |  | [optional]
**receivers** | Option<**Vec<String>**> |  | [optional]
**sender** | Option<**String**> |  | [optional]
**title** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


