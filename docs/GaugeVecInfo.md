# GaugeVecInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**method** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**throughput** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


