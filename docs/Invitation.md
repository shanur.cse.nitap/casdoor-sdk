# Invitation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application** | Option<**String**> |  | [optional]
**code** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**email** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**phone** | Option<**String**> |  | [optional]
**quota** | Option<**i64**> |  | [optional]
**signup_group** | Option<**String**> |  | [optional]
**state** | Option<**String**> |  | [optional]
**updated_time** | Option<**String**> |  | [optional]
**used_count** | Option<**i64**> |  | [optional]
**username** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


