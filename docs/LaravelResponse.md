# ControllersLaravelResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_at** | Option<**String**> |  | [optional]
**email** | Option<**String**> |  | [optional]
**email_verified_at** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**updated_at** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


