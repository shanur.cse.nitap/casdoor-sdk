# PpPeriodPaymentState

## Enum Variants

| Name | Value |
|---- | -----|
| PaymentStatePaidEqualDoubleQuotePaidDoubleQuote | PaymentStatePaid &#x3D; \&quot;Paid\&quot; |
| PaymentStateCreatedEqualDoubleQuoteCreatedDoubleQuote | PaymentStateCreated &#x3D; \&quot;Created\&quot; |
| PaymentStateCanceledEqualDoubleQuoteCanceledDoubleQuote | PaymentStateCanceled &#x3D; \&quot;Canceled\&quot; |
| PaymentStateTimeoutEqualDoubleQuoteTimeoutDoubleQuote | PaymentStateTimeout &#x3D; \&quot;Timeout\&quot; |
| PaymentStateErrorEqualDoubleQuoteErrorDoubleQuote | PaymentStateError &#x3D; \&quot;Error\&quot; |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


