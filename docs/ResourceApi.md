# casdoor\ResourceApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_resource**](ResourceApi.md#add_resource) | **POST** /api/add-resource | 
[**delete_resource**](ResourceApi.md#delete_resource) | **POST** /api/delete-resource | 
[**get_resource**](ResourceApi.md#get_resource) | **GET** /api/get-resource | 
[**get_resources**](ResourceApi.md#get_resources) | **GET** /api/get-resources | 
[**update_resource**](ResourceApi.md#update_resource) | **POST** /api/update-resource | 
[**upload_resource**](ResourceApi.md#upload_resource) | **POST** /api/upload-resource | 



## add_resource

> models::ControllersResponse add_resource(resource)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**resource** | [**Resource**](Resource.md) | Resource object | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_resource

> models::ControllersResponse delete_resource(resource)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**resource** | [**Resource**](Resource.md) | Resource object | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_resource

> models::Resource get_resource(id)


get resource

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of resource | [required] |

### Return type

[**models::Resource**](object.Resource.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_resources

> Vec<models::Resource> get_resources(owner, user, page_size, p, field, value, sort_field, sort_order)


get resources

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | Owner | [required] |
**user** | **String** | User | [required] |
**page_size** | Option<**i32**> | Page Size |  |
**p** | Option<**i32**> | Page Number |  |
**field** | Option<**String**> | Field |  |
**value** | Option<**String**> | Value |  |
**sort_field** | Option<**String**> | Sort Field |  |
**sort_order** | Option<**String**> | Sort Order |  |

### Return type

[**Vec<models::Resource>**](object.Resource.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_resource

> models::ControllersResponse update_resource(id, resource)


get resource

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of resource | [required] |
**resource** | [**Resource**](Resource.md) | The resource object | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## upload_resource

> models::Resource upload_resource(owner, user, application, full_file_path, file, tag, parent, created_time, description)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | Owner | [required] |
**user** | **String** | User | [required] |
**application** | **String** | Application | [required] |
**full_file_path** | **String** | Full File Path | [required] |
**file** | **std::path::PathBuf** | Resource file | [required] |
**tag** | Option<**String**> | Tag |  |
**parent** | Option<**String**> | Parent |  |
**created_time** | Option<**String**> | Created Time |  |
**description** | Option<**String**> | Description |  |

### Return type

[**models::Resource**](object.Resource.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

