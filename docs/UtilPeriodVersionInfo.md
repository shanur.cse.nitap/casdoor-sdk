# UtilPeriodVersionInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**commit_id** | Option<**String**> |  | [optional]
**commit_offset** | Option<**i64**> |  | [optional]
**version** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


