# Organization

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_items** | Option<[**Vec<models::AccountItem>**](object.AccountItem.md)> |  | [optional]
**country_codes** | Option<**Vec<String>**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**default_application** | Option<**String**> |  | [optional]
**default_avatar** | Option<**String**> |  | [optional]
**default_password** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**enable_soft_deletion** | Option<**bool**> |  | [optional]
**favicon** | Option<**String**> |  | [optional]
**init_score** | Option<**i64**> |  | [optional]
**is_profile_public** | Option<**bool**> |  | [optional]
**languages** | Option<**Vec<String>**> |  | [optional]
**master_password** | Option<**String**> |  | [optional]
**master_verification_code** | Option<**String**> |  | [optional]
**mfa_items** | Option<[**Vec<models::MfaItem>**](object.MfaItem.md)> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**password_options** | Option<**Vec<String>**> |  | [optional]
**password_salt** | Option<**String**> |  | [optional]
**password_type** | Option<**String**> |  | [optional]
**tags** | Option<**Vec<String>**> |  | [optional]
**theme_data** | Option<[**models::ThemeData**](object.ThemeData.md)> |  | [optional]
**website_url** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


