# HistogramVecInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | Option<**i64**> |  | [optional]
**latency** | Option<**String**> |  | [optional]
**method** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


