# Userinfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | Option<**String**> |  | [optional]
**aud** | Option<**String**> |  | [optional]
**email** | Option<**String**> |  | [optional]
**email_verified** | Option<**bool**> |  | [optional]
**groups** | Option<**Vec<String>**> |  | [optional]
**iss** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**phone** | Option<**String**> |  | [optional]
**picture** | Option<**String**> |  | [optional]
**preferred_username** | Option<**String**> |  | [optional]
**sub** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


