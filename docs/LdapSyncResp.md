# ControllersLdapSyncResp

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exist** | Option<[**Vec<models::LdapUser>**](object.LdapUser.md)> |  | [optional]
**failed** | Option<[**Vec<models::LdapUser>**](object.LdapUser.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


