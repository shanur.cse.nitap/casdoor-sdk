# casdoor\SubscriptionApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_subscription**](SubscriptionApi.md#add_subscription) | **POST** /api/add-subscription | 
[**delete_subscription**](SubscriptionApi.md#delete_subscription) | **POST** /api/delete-subscription | 
[**get_subscription**](SubscriptionApi.md#get_subscription) | **GET** /api/get-subscription | 
[**get_subscriptions**](SubscriptionApi.md#get_subscriptions) | **GET** /api/get-subscriptions | 
[**update_subscription**](SubscriptionApi.md#update_subscription) | **POST** /api/update-subscription | 



## add_subscription

> models::ControllersResponse add_subscription(body)


add subscription

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Subscription**](Subscription.md) | The details of the subscription | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_subscription

> models::ControllersResponse delete_subscription(body)


delete subscription

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Subscription**](Subscription.md) | The details of the subscription | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_subscription

> models::Subscription get_subscription(id)


get subscription

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the subscription | [required] |

### Return type

[**models::Subscription**](object.Subscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_subscriptions

> Vec<models::Subscription> get_subscriptions(owner)


get subscriptions

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of subscriptions | [required] |

### Return type

[**Vec<models::Subscription>**](object.Subscription.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_subscription

> models::ControllersResponse update_subscription(id, body)


update subscription

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the subscription | [required] |
**body** | [**Subscription**](Subscription.md) | The details of the subscription | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

