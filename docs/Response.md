# ControllersResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | Option<[**serde_json::Value**](.md)> |  | [optional]
**data2** | Option<[**serde_json::Value**](.md)> |  | [optional]
**msg** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**status** | Option<**String**> |  | [optional]
**sub** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


