# SignupItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**label** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**placeholder** | Option<**String**> |  | [optional]
**prompted** | Option<**bool**> |  | [optional]
**regex** | Option<**String**> |  | [optional]
**required** | Option<**bool**> |  | [optional]
**rule** | Option<**String**> |  | [optional]
**visible** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


