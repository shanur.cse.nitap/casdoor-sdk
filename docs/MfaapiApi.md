# casdoor\MfaApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**delete_mfa**](MfaApi.md#delete_mfa) | **POST** /api/delete-mfa/ | 
[**mfa_setup_enable**](MfaApi.md#mfa_setup_enable) | **POST** /api/mfa/setup/enable | 
[**mfa_setup_initiate**](MfaApi.md#mfa_setup_initiate) | **POST** /api/mfa/setup/initiate | 
[**mfa_setup_verify**](MfaApi.md#mfa_setup_verify) | **POST** /api/mfa/setup/verify | 
[**set_preferred_mfa**](MfaApi.md#set_preferred_mfa) | **POST** /api/set-preferred-mfa | 



## delete_mfa

> models::ControllersResponse delete_mfa()


: Delete MFA

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## mfa_setup_enable

> models::ControllersResponse mfa_setup_enable()


enable totp

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## mfa_setup_initiate

> models::ControllersResponse mfa_setup_initiate()


setup MFA

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## mfa_setup_verify

> models::ControllersResponse mfa_setup_verify()


setup verify totp

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## set_preferred_mfa

> models::ControllersResponse set_preferred_mfa()


: Set specific Mfa Preferred

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

