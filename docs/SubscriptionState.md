# SubscriptionState

## Enum Variants

| Name | Value |
|---- | -----|
| SubStatePendingEqualDoubleQuotePendingDoubleQuote | SubStatePending &#x3D; \&quot;Pending\&quot; |
| SubStateErrorEqualDoubleQuoteErrorDoubleQuote | SubStateError &#x3D; \&quot;Error\&quot; |
| SubStateSuspendedEqualDoubleQuoteSuspendedDoubleQuote | SubStateSuspended &#x3D; \&quot;Suspended\&quot; |
| SubStateActiveEqualDoubleQuoteActiveDoubleQuote | SubStateActive &#x3D; \&quot;Active\&quot; |
| SubStateUpcomingEqualDoubleQuoteUpcomingDoubleQuote | SubStateUpcoming &#x3D; \&quot;Upcoming\&quot; |
| SubStateExpiredEqualDoubleQuoteExpiredDoubleQuote | SubStateExpired &#x3D; \&quot;Expired\&quot; |


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


