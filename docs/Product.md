# Product

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_time** | Option<**String**> |  | [optional]
**currency** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**detail** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**image** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**price** | Option<**f64**> |  | [optional]
**provider_objs** | Option<[**Vec<models::Provider>**](object.Provider.md)> |  | [optional]
**providers** | Option<**Vec<String>**> |  | [optional]
**quantity** | Option<**i64**> |  | [optional]
**return_url** | Option<**String**> |  | [optional]
**sold** | Option<**i64**> |  | [optional]
**state** | Option<**String**> |  | [optional]
**tag** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


