# Cert

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bit_size** | Option<**i64**> |  | [optional]
**certificate** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**crypto_algorithm** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**expire_in_years** | Option<**i64**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**private_key** | Option<**String**> |  | [optional]
**scope** | Option<**String**> |  | [optional]
**r#type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


