# casdoor\AdapterApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_adapter**](AdapterApi.md#add_adapter) | **POST** /api/add-adapter | 
[**delete_adapter**](AdapterApi.md#delete_adapter) | **POST** /api/delete-adapter | 
[**get_adapter**](AdapterApi.md#get_adapter) | **GET** /api/get-adapter | 
[**get_adapters**](AdapterApi.md#get_adapters) | **GET** /api/get-adapters | 
[**update_adapter**](AdapterApi.md#update_adapter) | **POST** /api/update-adapter | 



## add_adapter

> models::ControllersResponse add_adapter(body)


add adapter

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Adapter**](Adapter.md) | The details of the adapter | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_adapter

> models::ControllersResponse delete_adapter(body)


delete adapter

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Adapter**](Adapter.md) | The details of the adapter | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_adapter

> models::Adapter get_adapter(id)


get adapter

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the adapter | [required] |

### Return type

[**models::Adapter**](object.Adapter.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_adapters

> Vec<models::Adapter> get_adapters(owner)


get adapters

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of adapters | [required] |

### Return type

[**Vec<models::Adapter>**](object.Adapter.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_adapter

> models::ControllersResponse update_adapter(id, body)


update adapter

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the adapter | [required] |
**body** | [**Adapter**](Adapter.md) | The details of the adapter | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

