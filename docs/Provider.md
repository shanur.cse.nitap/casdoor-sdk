# Provider

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**app_id** | Option<**String**> |  | [optional]
**bucket** | Option<**String**> |  | [optional]
**category** | Option<**String**> |  | [optional]
**cert** | Option<**String**> |  | [optional]
**client_id** | Option<**String**> |  | [optional]
**client_id2** | Option<**String**> |  | [optional]
**client_secret** | Option<**String**> |  | [optional]
**client_secret2** | Option<**String**> |  | [optional]
**content** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**custom_auth_url** | Option<**String**> |  | [optional]
**custom_logo** | Option<**String**> |  | [optional]
**custom_token_url** | Option<**String**> |  | [optional]
**custom_user_info_url** | Option<**String**> |  | [optional]
**disable_ssl** | Option<**bool**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**domain** | Option<**String**> |  | [optional]
**enable_sign_authn_request** | Option<**bool**> |  | [optional]
**endpoint** | Option<**String**> |  | [optional]
**host** | Option<**String**> |  | [optional]
**id_p** | Option<**String**> |  | [optional]
**intranet_endpoint** | Option<**String**> |  | [optional]
**issuer_url** | Option<**String**> |  | [optional]
**metadata** | Option<**String**> |  | [optional]
**method** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**path_prefix** | Option<**String**> |  | [optional]
**port** | Option<**i64**> |  | [optional]
**provider_url** | Option<**String**> |  | [optional]
**receiver** | Option<**String**> |  | [optional]
**region_id** | Option<**String**> |  | [optional]
**scopes** | Option<**String**> |  | [optional]
**sign_name** | Option<**String**> |  | [optional]
**sub_type** | Option<**String**> |  | [optional]
**template_code** | Option<**String**> |  | [optional]
**title** | Option<**String**> |  | [optional]
**r#type** | Option<**String**> |  | [optional]
**user_mapping** | Option<[**serde_json::Value**](.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


