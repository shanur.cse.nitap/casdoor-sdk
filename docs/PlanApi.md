# casdoor\PlanApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_plan**](PlanApi.md#add_plan) | **POST** /api/add-plan | 
[**delete_plan**](PlanApi.md#delete_plan) | **POST** /api/delete-plan | 
[**get_plan**](PlanApi.md#get_plan) | **GET** /api/get-plan | 
[**get_plans**](PlanApi.md#get_plans) | **GET** /api/get-plans | 
[**update_plan**](PlanApi.md#update_plan) | **POST** /api/update-plan | 



## add_plan

> models::ControllersResponse add_plan(body)


add plan

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Plan**](Plan.md) | The details of the plan | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_plan

> models::ControllersResponse delete_plan(body)


delete plan

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Plan**](Plan.md) | The details of the plan | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_plan

> models::Plan get_plan(id, include_option)


get plan

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the plan | [required] |
**include_option** | Option<**bool**> | Should include plan's option |  |

### Return type

[**models::Plan**](object.Plan.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_plans

> Vec<models::Plan> get_plans(owner)


get plans

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of plans | [required] |

### Return type

[**Vec<models::Plan>**](object.Plan.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_plan

> models::ControllersResponse update_plan(id, body)


update plan

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the plan | [required] |
**body** | [**Plan**](Plan.md) | The details of the plan | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

