# Pricing

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**is_enabled** | Option<**bool**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**plans** | Option<**Vec<String>**> |  | [optional]
**trial_duration** | Option<**i64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


