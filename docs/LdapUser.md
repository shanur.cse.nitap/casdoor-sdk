# LdapUser

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**email_address** | Option<**String**> |  | [optional]
**mail** | Option<**String**> |  | [optional]
**mobile_telephone_number** | Option<**String**> |  | [optional]
**postal_address** | Option<**String**> |  | [optional]
**registered_address** | Option<**String**> |  | [optional]
**telephone_number** | Option<**String**> |  | [optional]
**address** | Option<**String**> |  | [optional]
**cn** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**email** | Option<**String**> |  | [optional]
**gid_number** | Option<**String**> |  | [optional]
**group_id** | Option<**String**> |  | [optional]
**member_of** | Option<**String**> |  | [optional]
**mobile** | Option<**String**> |  | [optional]
**uid** | Option<**String**> |  | [optional]
**uid_number** | Option<**String**> |  | [optional]
**user_principal_name** | Option<**String**> |  | [optional]
**uuid** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


