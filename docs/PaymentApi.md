# casdoor\PaymentApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_payment**](PaymentApi.md#add_payment) | **POST** /api/add-payment | 
[**delete_payment**](PaymentApi.md#delete_payment) | **POST** /api/delete-payment | 
[**get_payment**](PaymentApi.md#get_payment) | **GET** /api/get-payment | 
[**get_payments**](PaymentApi.md#get_payments) | **GET** /api/get-payments | 
[**get_user_payments**](PaymentApi.md#get_user_payments) | **GET** /api/get-user-payments | 
[**invoice_payment**](PaymentApi.md#invoice_payment) | **POST** /api/invoice-payment | 
[**notify_payment**](PaymentApi.md#notify_payment) | **POST** /api/notify-payment | 
[**update_payment**](PaymentApi.md#update_payment) | **POST** /api/update-payment | 



## add_payment

> models::ControllersResponse add_payment(body)


add payment

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Payment**](Payment.md) | The details of the payment | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_payment

> models::ControllersResponse delete_payment(body)


delete payment

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Payment**](Payment.md) | The details of the payment | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_payment

> models::Payment get_payment(id)


get payment

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the payment | [required] |

### Return type

[**models::Payment**](object.Payment.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_payments

> Vec<models::Payment> get_payments(owner)


get payments

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of payments | [required] |

### Return type

[**Vec<models::Payment>**](object.Payment.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_user_payments

> Vec<models::Payment> get_user_payments(owner, organization, user)


get payments for a user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of payments | [required] |
**organization** | **String** | The organization of the user | [required] |
**user** | **String** | The username of the user | [required] |

### Return type

[**Vec<models::Payment>**](object.Payment.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## invoice_payment

> models::ControllersResponse invoice_payment(id)


invoice payment

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the payment | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## notify_payment

> models::ControllersResponse notify_payment(body)


notify payment

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Payment**](Payment.md) | The details of the payment | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_payment

> models::ControllersResponse update_payment(id, body)


update payment

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the payment | [required] |
**body** | [**Payment**](Payment.md) | The details of the payment | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

