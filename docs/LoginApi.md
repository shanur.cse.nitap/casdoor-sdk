# casdoor\LoginApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_application_login**](LoginApi.md#get_application_login) | **GET** /api/get-app-login | 
[**get_captcha**](LoginApi.md#get_captcha) | **GET** /api/get-captcha | 
[**introspect_token**](LoginApi.md#introspect_token) | **POST** /api/login/oauth/introspect | 
[**login**](LoginApi.md#login) | **POST** /api/login | 
[**logout**](LoginApi.md#logout) | **POST** /api/logout | 
[**signup**](LoginApi.md#signup) | **POST** /api/signup | 
[**unlink**](LoginApi.md#unlink) | **POST** /api/unlink | 
[**web_authn_signin_begin**](LoginApi.md#web_authn_signin_begin) | **GET** /api/webauthn/signin/begin | 
[**web_authn_signin_finish**](LoginApi.md#web_authn_signin_finish) | **POST** /api/webauthn/signin/finish | 



## get_application_login

> models::ControllersResponse get_application_login(client_id, response_type, redirect_uri, scope, state)


get application login

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**client_id** | **String** | client id | [required] |
**response_type** | **String** | response type | [required] |
**redirect_uri** | **String** | redirect uri | [required] |
**scope** | **String** | scope | [required] |
**state** | **String** | state | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_captcha

> models::Userinfo get_captcha()


### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## introspect_token

> models::IntrospectionResponse introspect_token(token, token_type_hint)


The introspection endpoint is an OAuth 2.0 endpoint that takes a

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**token** | **String** | access_token's value or refresh_token's value | [required] |
**token_type_hint** | **String** | the token type access_token or refresh_token | [required] |

### Return type

[**models::IntrospectionResponse**](object.IntrospectionResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## login

> models::ControllersResponse login(client_id, response_type, redirect_uri, form, scope, state, nonce, code_challenge_method, code_challenge)


login

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**client_id** | **String** | clientId | [required] |
**response_type** | **String** | responseType | [required] |
**redirect_uri** | **String** | redirectUri | [required] |
**form** | **serde_json::Value** | Login information | [required] |
**scope** | Option<**String**> | scope |  |
**state** | Option<**String**> | state |  |
**nonce** | Option<**String**> | nonce |  |
**code_challenge_method** | Option<**String**> | code_challenge_method |  |
**code_challenge** | Option<**String**> | code_challenge |  |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## logout

> models::ControllersResponse logout(id_token_hint, post_logout_redirect_uri, state)


logout the current user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id_token_hint** | Option<**String**> | id_token_hint |  |
**post_logout_redirect_uri** | Option<**String**> | post_logout_redirect_uri |  |
**state** | Option<**String**> | state |  |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## signup

> models::ControllersResponse signup(username, password)


sign up a new user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**username** | **String** | The username to sign up | [required] |
**password** | **String** | The password | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## unlink

> models::Userinfo unlink()


### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## web_authn_signin_begin

> serde_json::Value web_authn_signin_begin(owner, name)


WebAuthn Login Flow 1st stage

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | owner | [required] |
**name** | **String** | name | [required] |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## web_authn_signin_finish

> models::ControllersResponse web_authn_signin_finish(body)


WebAuthn Login Flow 2nd stage

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | **serde_json::Value** | authenticator assertion Response | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

