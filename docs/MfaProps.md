# MfaProps

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country_code** | Option<**String**> |  | [optional]
**enabled** | Option<**bool**> |  | [optional]
**is_preferred** | Option<**bool**> |  | [optional]
**mfa_type** | Option<**String**> |  | [optional]
**recovery_codes** | Option<**Vec<String>**> |  | [optional]
**secret** | Option<**String**> |  | [optional]
**url** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


