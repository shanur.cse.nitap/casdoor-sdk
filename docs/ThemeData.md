# ThemeData

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**border_radius** | Option<**i64**> |  | [optional]
**color_primary** | Option<**String**> |  | [optional]
**is_compact** | Option<**bool**> |  | [optional]
**is_enabled** | Option<**bool**> |  | [optional]
**theme_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


