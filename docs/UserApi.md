# casdoor\UserApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_user**](UserApi.md#add_user) | **POST** /api/add-user | 
[**add_user_keys**](UserApi.md#add_user_keys) | **POST** /api/add-user-keys | 
[**check_user_password**](UserApi.md#check_user_password) | **POST** /api/check-user-password | 
[**delete_user**](UserApi.md#delete_user) | **POST** /api/delete-user | 
[**get_email_and_phone**](UserApi.md#get_email_and_phone) | **GET** /api/get-email-and-phone | 
[**get_global_users**](UserApi.md#get_global_users) | **GET** /api/get-global-users | 
[**get_sorted_users**](UserApi.md#get_sorted_users) | **GET** /api/get-sorted-users | 
[**get_user**](UserApi.md#get_user) | **GET** /api/get-user | 
[**get_user_count**](UserApi.md#get_user_count) | **GET** /api/get-user-count | 
[**get_users**](UserApi.md#get_users) | **GET** /api/get-users | 
[**update_user**](UserApi.md#update_user) | **POST** /api/update-user | 
[**web_authn_signup_begin**](UserApi.md#web_authn_signup_begin) | **GET** /api/webauthn/signup/begin | 
[**web_authn_signup_finish**](UserApi.md#web_authn_signup_finish) | **POST** /api/webauthn/signup/finish | 



## add_user

> models::ControllersResponse add_user(body)


add user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**User**](User.md) | The details of the user | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## add_user_keys

> models::Userinfo add_user_keys()


### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## check_user_password

> models::Userinfo check_user_password()


### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_user

> models::ControllersResponse delete_user(body)


delete user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**User**](User.md) | The details of the user | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_email_and_phone

> models::ControllersResponse get_email_and_phone(username, organization)


get email and phone by username

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**username** | **String** | The username of the user | [required] |
**organization** | **String** | The organization of the user | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: multipart/form-data
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_global_users

> Vec<models::User> get_global_users()


get global users

### Parameters

This endpoint does not need any parameter.

### Return type

[**Vec<models::User>**](object.User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_sorted_users

> Vec<models::User> get_sorted_users(owner, sorter, limit)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of users | [required] |
**sorter** | **String** | The DB column name to sort by, e.g., created_time | [required] |
**limit** | **String** | The count of users to return, e.g., 25 | [required] |

### Return type

[**Vec<models::User>**](object.User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_user

> models::User get_user(id, owner, email, phone, user_id)


get user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | Option<**String**> | The id ( owner/name ) of the user |  |
**owner** | Option<**String**> | The owner of the user |  |
**email** | Option<**String**> | The email of the user |  |
**phone** | Option<**String**> | The phone of the user |  |
**user_id** | Option<**String**> | The userId of the user |  |

### Return type

[**models::User**](object.User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_user_count

> get_user_count(owner, is_online)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of users | [required] |
**is_online** | **String** | The filter for query, 1 for online, 0 for offline, empty string for all users | [required] |

### Return type

 (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_users

> Vec<models::User> get_users(owner)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of users | [required] |

### Return type

[**Vec<models::User>**](object.User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_user

> models::ControllersResponse update_user(id, body)


update user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the user | [required] |
**body** | [**User**](User.md) | The details of the user | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## web_authn_signup_begin

> serde_json::Value web_authn_signup_begin()


WebAuthn Registration Flow 1st stage

### Parameters

This endpoint does not need any parameter.

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## web_authn_signup_finish

> models::ControllersResponse web_authn_signup_finish(body)


WebAuthn Registration Flow 2nd stage

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | **serde_json::Value** | authenticator attestation Response | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

