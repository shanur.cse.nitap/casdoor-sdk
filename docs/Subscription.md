# Subscription

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_time** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**end_time** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**payment** | Option<**String**> |  | [optional]
**period** | Option<**String**> |  | [optional]
**plan** | Option<**String**> |  | [optional]
**pricing** | Option<**String**> |  | [optional]
**start_time** | Option<**String**> |  | [optional]
**state** | Option<[**models::SubscriptionState**](object.SubscriptionState.md)> |  | [optional]
**user** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


