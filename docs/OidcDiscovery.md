# OidcDiscovery

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorization_endpoint** | Option<**String**> |  | [optional]
**claims_supported** | Option<**Vec<String>**> |  | [optional]
**end_session_endpoint** | Option<**String**> |  | [optional]
**grant_types_supported** | Option<**Vec<String>**> |  | [optional]
**id_token_signing_alg_values_supported** | Option<**Vec<String>**> |  | [optional]
**introspection_endpoint** | Option<**String**> |  | [optional]
**issuer** | Option<**String**> |  | [optional]
**jwks_uri** | Option<**String**> |  | [optional]
**request_object_signing_alg_values_supported** | Option<**Vec<String>**> |  | [optional]
**request_parameter_supported** | Option<**bool**> |  | [optional]
**response_modes_supported** | Option<**Vec<String>**> |  | [optional]
**response_types_supported** | Option<**Vec<String>**> |  | [optional]
**scopes_supported** | Option<**Vec<String>**> |  | [optional]
**subject_types_supported** | Option<**Vec<String>**> |  | [optional]
**token_endpoint** | Option<**String**> |  | [optional]
**userinfo_endpoint** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


