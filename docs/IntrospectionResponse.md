# IntrospectionResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | Option<**bool**> |  | [optional]
**aud** | Option<**Vec<String>**> |  | [optional]
**client_id** | Option<**String**> |  | [optional]
**exp** | Option<**i64**> |  | [optional]
**iat** | Option<**i64**> |  | [optional]
**iss** | Option<**String**> |  | [optional]
**jti** | Option<**String**> |  | [optional]
**nbf** | Option<**i64**> |  | [optional]
**scope** | Option<**String**> |  | [optional]
**sub** | Option<**String**> |  | [optional]
**token_type** | Option<**String**> |  | [optional]
**username** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


