# ProviderItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**can_sign_in** | Option<**bool**> |  | [optional]
**can_sign_up** | Option<**bool**> |  | [optional]
**can_unlink** | Option<**bool**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**prompted** | Option<**bool**> |  | [optional]
**provider** | Option<[**models::Provider**](object.Provider.md)> |  | [optional]
**rule** | Option<**String**> |  | [optional]
**signup_group** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


