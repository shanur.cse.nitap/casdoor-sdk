# UtilPeriodSystemInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cpu_usage** | Option<**Vec<f64>**> |  | [optional]
**memory_total** | Option<**i64**> |  | [optional]
**memory_used** | Option<**i64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


