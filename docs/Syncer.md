# Syncer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**affiliation_table** | Option<**String**> |  | [optional]
**avatar_base_url** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**database** | Option<**String**> |  | [optional]
**database_type** | Option<**String**> |  | [optional]
**error_text** | Option<**String**> |  | [optional]
**host** | Option<**String**> |  | [optional]
**is_enabled** | Option<**bool**> |  | [optional]
**is_read_only** | Option<**bool**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**organization** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**password** | Option<**String**> |  | [optional]
**port** | Option<**i64**> |  | [optional]
**ssl_mode** | Option<**String**> |  | [optional]
**sync_interval** | Option<**i64**> |  | [optional]
**table** | Option<**String**> |  | [optional]
**table_columns** | Option<[**Vec<models::TableColumn>**](object.TableColumn.md)> |  | [optional]
**r#type** | Option<**String**> |  | [optional]
**user** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


