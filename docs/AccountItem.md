# AccountItem

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**modify_rule** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**view_rule** | Option<**String**> |  | [optional]
**visible** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


