# casdoor\SystemApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_dashboard**](SystemApi.md#get_dashboard) | **GET** /api/get-dashboard | 
[**get_prometheus_info**](SystemApi.md#get_prometheus_info) | **GET** /api/get-prometheus-info | 
[**get_system_info**](SystemApi.md#get_system_info) | **GET** /api/get-system-info | 
[**get_version_info**](SystemApi.md#get_version_info) | **GET** /api/get-version-info | 
[**get_webhook_event_type**](SystemApi.md#get_webhook_event_type) | **GET** /api/get-webhook-event | 
[**handle_official_account_event**](SystemApi.md#handle_official_account_event) | **POST** /api/webhook | 
[**health**](SystemApi.md#health) | **GET** /api/health | 



## get_dashboard

> models::ControllersResponse get_dashboard()


get information of dashboard

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_prometheus_info

> models::PrometheusInfo get_prometheus_info()


get Prometheus Info

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::PrometheusInfo**](object.PrometheusInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_system_info

> models::UtilPeriodSystemInfo get_system_info()


get system info like CPU and memory usage

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::UtilPeriodSystemInfo**](util.SystemInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_version_info

> models::UtilPeriodVersionInfo get_version_info()


get version info like Casdoor release version and commit ID

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::UtilPeriodVersionInfo**](util.VersionInfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_webhook_event_type

> models::Userinfo get_webhook_event_type()


### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## handle_official_account_event

> models::Userinfo handle_official_account_event()


### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## health

> models::ControllersResponse health()


check if the system is live

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

