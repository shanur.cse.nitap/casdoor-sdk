# Group

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**children** | Option<[**Vec<models::Group>**](object.Group.md)> |  | [optional]
**contact_email** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**is_enabled** | Option<**bool**> |  | [optional]
**is_top_group** | Option<**bool**> |  | [optional]
**key** | Option<**String**> |  | [optional]
**manager** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**parent_id** | Option<**String**> |  | [optional]
**title** | Option<**String**> |  | [optional]
**r#type** | Option<**String**> |  | [optional]
**updated_time** | Option<**String**> |  | [optional]
**users** | Option<[**Vec<models::User>**](object.User.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


