# Plan

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_time** | Option<**String**> |  | [optional]
**currency** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**is_enabled** | Option<**bool**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**options** | Option<**Vec<String>**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**payment_providers** | Option<**Vec<String>**> |  | [optional]
**period** | Option<**String**> |  | [optional]
**price** | Option<**f64**> |  | [optional]
**product** | Option<**String**> |  | [optional]
**role** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


