# casdoor\EnforcerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_enforcer**](EnforcerApi.md#add_enforcer) | **POST** /api/add-enforcer | 
[**batch_enforce**](EnforcerApi.md#batch_enforce) | **POST** /api/batch-enforce | 
[**delete_enforcer**](EnforcerApi.md#delete_enforcer) | **POST** /api/delete-enforcer | 
[**enforce**](EnforcerApi.md#enforce) | **POST** /api/enforce | 
[**get_enforcer**](EnforcerApi.md#get_enforcer) | **GET** /api/get-enforcer | 
[**get_enforcers**](EnforcerApi.md#get_enforcers) | **GET** /api/get-enforcers | 
[**update_enforcer**](EnforcerApi.md#update_enforcer) | **POST** /api/update-enforcer | 



## add_enforcer

> models::Enforcer add_enforcer(enforcer)


add enforcer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**enforcer** | **serde_json::Value** | The enforcer object | [required] |

### Return type

[**models::Enforcer**](object.Enforcer.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## batch_enforce

> models::ControllersResponse batch_enforce(body, permission_id, model_id, owner)


Call Casbin BatchEnforce API

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Vec<String>**](String.md) | array of casbin requests | [required] |
**permission_id** | Option<**String**> | permission id |  |
**model_id** | Option<**String**> | model id |  |
**owner** | Option<**String**> | owner |  |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_enforcer

> models::Enforcer delete_enforcer(body)


delete enforcer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Enforcer**](Enforcer.md) | The enforcer object | [required] |

### Return type

[**models::Enforcer**](object.Enforcer.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## enforce

> models::ControllersResponse enforce(body, permission_id, model_id, resource_id, owner)


Call Casbin Enforce API

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Vec<String>**](String.md) | Casbin request | [required] |
**permission_id** | Option<**String**> | permission id |  |
**model_id** | Option<**String**> | model id |  |
**resource_id** | Option<**String**> | resource id |  |
**owner** | Option<**String**> | owner |  |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_enforcer

> models::Enforcer get_enforcer(id)


get enforcer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name )  of enforcer | [required] |

### Return type

[**models::Enforcer**](object.Enforcer.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_enforcers

> Vec<models::Enforcer> get_enforcers(owner)


get enforcers

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of enforcers | [required] |

### Return type

[**Vec<models::Enforcer>**](object.Enforcer.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_enforcer

> models::Enforcer update_enforcer(id, enforcer)


update enforcer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name )  of enforcer | [required] |
**enforcer** | **serde_json::Value** | The enforcer object | [required] |

### Return type

[**models::Enforcer**](object.Enforcer.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

