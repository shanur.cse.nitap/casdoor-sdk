# Payment

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_time** | Option<**String**> |  | [optional]
**currency** | Option<**String**> |  | [optional]
**detail** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**invoice_remark** | Option<**String**> |  | [optional]
**invoice_tax_id** | Option<**String**> |  | [optional]
**invoice_title** | Option<**String**> |  | [optional]
**invoice_type** | Option<**String**> |  | [optional]
**invoice_url** | Option<**String**> |  | [optional]
**message** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**out_order_id** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**pay_url** | Option<**String**> |  | [optional]
**person_email** | Option<**String**> |  | [optional]
**person_id_card** | Option<**String**> |  | [optional]
**person_name** | Option<**String**> |  | [optional]
**person_phone** | Option<**String**> |  | [optional]
**price** | Option<**f64**> |  | [optional]
**product_display_name** | Option<**String**> |  | [optional]
**product_name** | Option<**String**> |  | [optional]
**provider** | Option<**String**> |  | [optional]
**return_url** | Option<**String**> |  | [optional]
**state** | Option<[**models::PpPeriodPaymentState**](pp.PaymentState.md)> |  | [optional]
**success_url** | Option<**String**> |  | [optional]
**tag** | Option<**String**> |  | [optional]
**r#type** | Option<**String**> |  | [optional]
**user** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


