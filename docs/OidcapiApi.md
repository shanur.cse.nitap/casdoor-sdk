# casdoor\OidcApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**root_controllerget_jwks**](OidcApi.md#root_controllerget_jwks) | **GET** /.well-known/jwks | 
[**root_controllerget_oidc_discovery**](OidcApi.md#root_controllerget_oidc_discovery) | **GET** /.well-known/openid-configuration | 



## root_controllerget_jwks

> serde_json::Value root_controllerget_jwks()


### Parameters

This endpoint does not need any parameter.

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## root_controllerget_oidc_discovery

> models::OidcDiscovery root_controllerget_oidc_discovery()


Get Oidc Discovery

### Parameters

This endpoint does not need any parameter.

### Return type

[**models::OidcDiscovery**](object.OidcDiscovery.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

