# Ormer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**engine** | Option<[**serde_json::Value**](.md)> |  | [optional]
**data_source_name** | Option<**String**> |  | [optional]
**db_name** | Option<**String**> |  | [optional]
**driver_name** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


