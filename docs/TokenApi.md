# casdoor\TokenApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_token**](TokenApi.md#add_token) | **POST** /api/add-token | 
[**delete_token**](TokenApi.md#delete_token) | **POST** /api/delete-token | 
[**get_captcha_status**](TokenApi.md#get_captcha_status) | **GET** /api/get-captcha-status | 
[**get_o_auth_token**](TokenApi.md#get_o_auth_token) | **POST** /api/login/oauth/access_token | 
[**get_token**](TokenApi.md#get_token) | **GET** /api/get-token | 
[**get_tokens**](TokenApi.md#get_tokens) | **GET** /api/get-tokens | 
[**refresh_token**](TokenApi.md#refresh_token) | **POST** /api/login/oauth/refresh_token | 
[**update_token**](TokenApi.md#update_token) | **POST** /api/update-token | 



## add_token

> models::ControllersResponse add_token(body)


add token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Token**](Token.md) | Details of the token | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_token

> models::ControllersResponse delete_token(body)


delete token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Token**](Token.md) | Details of the token | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_captcha_status

> models::ControllersResponse get_captcha_status(id)


Get Login Error Counts

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of user | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_o_auth_token

> models::TokenWrapper get_o_auth_token(grant_type, client_id, client_secret, code)


get OAuth access token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**grant_type** | **String** | OAuth grant type | [required] |
**client_id** | **String** | OAuth client id | [required] |
**client_secret** | **String** | OAuth client secret | [required] |
**code** | **String** | OAuth code | [required] |

### Return type

[**models::TokenWrapper**](object.TokenWrapper.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_token

> models::Token get_token(id)


get token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of token | [required] |

### Return type

[**models::Token**](object.Token.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_tokens

> Vec<models::Token> get_tokens(owner, page_size, p)


get tokens

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of tokens | [required] |
**page_size** | **String** | The size of each page | [required] |
**p** | **String** | The number of the page | [required] |

### Return type

[**Vec<models::Token>**](object.Token.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## refresh_token

> models::TokenWrapper refresh_token(grant_type, refresh_token, scope, client_id, client_secret)


refresh OAuth access token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**grant_type** | **String** | OAuth grant type | [required] |
**refresh_token** | **String** | OAuth refresh token | [required] |
**scope** | **String** | OAuth scope | [required] |
**client_id** | **String** | OAuth client id | [required] |
**client_secret** | Option<**String**> | OAuth client secret |  |

### Return type

[**models::TokenWrapper**](object.TokenWrapper.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_token

> models::ControllersResponse update_token(id, body)


update token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of token | [required] |
**body** | [**Token**](Token.md) | Details of the token | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

