# Ldap

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auto_sync** | Option<**i64**> |  | [optional]
**base_dn** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**enable_ssl** | Option<**bool**> |  | [optional]
**filter** | Option<**String**> |  | [optional]
**filter_fields** | Option<**Vec<String>**> |  | [optional]
**host** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**last_sync** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**password** | Option<**String**> |  | [optional]
**port** | Option<**i64**> |  | [optional]
**server_name** | Option<**String**> |  | [optional]
**username** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


