# PrometheusInfo

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**api_latency** | Option<[**Vec<models::HistogramVecInfo>**](object.HistogramVecInfo.md)> |  | [optional]
**api_throughput** | Option<[**Vec<models::GaugeVecInfo>**](object.GaugeVecInfo.md)> |  | [optional]
**total_throughput** | Option<**f64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


