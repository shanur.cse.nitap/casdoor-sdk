# Token

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | Option<**String**> |  | [optional]
**access_token_hash** | Option<**String**> |  | [optional]
**application** | Option<**String**> |  | [optional]
**code** | Option<**String**> |  | [optional]
**code_challenge** | Option<**String**> |  | [optional]
**code_expire_in** | Option<**i64**> |  | [optional]
**code_is_used** | Option<**bool**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**expires_in** | Option<**i64**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**organization** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**refresh_token** | Option<**String**> |  | [optional]
**refresh_token_hash** | Option<**String**> |  | [optional]
**scope** | Option<**String**> |  | [optional]
**token_type** | Option<**String**> |  | [optional]
**user** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


