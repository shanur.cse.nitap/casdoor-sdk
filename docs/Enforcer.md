# Enforcer

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**adapter** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**model** | Option<**String**> |  | [optional]
**model_cfg** | Option<[**serde_json::Value**](.md)> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**updated_time** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


