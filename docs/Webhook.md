# Webhook

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content_type** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**events** | Option<**Vec<String>**> |  | [optional]
**headers** | Option<[**Vec<models::Header>**](object.Header.md)> |  | [optional]
**is_enabled** | Option<**bool**> |  | [optional]
**is_user_extended** | Option<**bool**> |  | [optional]
**method** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**organization** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**url** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


