# Role

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_time** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**domains** | Option<**Vec<String>**> |  | [optional]
**groups** | Option<**Vec<String>**> |  | [optional]
**is_enabled** | Option<**bool**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**roles** | Option<**Vec<String>**> |  | [optional]
**users** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


