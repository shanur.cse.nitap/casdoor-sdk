# Resource

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**application** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**file_format** | Option<**String**> |  | [optional]
**file_name** | Option<**String**> |  | [optional]
**file_size** | Option<**i64**> |  | [optional]
**file_type** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**parent** | Option<**String**> |  | [optional]
**provider** | Option<**String**> |  | [optional]
**tag** | Option<**String**> |  | [optional]
**url** | Option<**String**> |  | [optional]
**user** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


