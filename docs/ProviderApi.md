# casdoor\ProviderApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_provider**](ProviderApi.md#add_provider) | **POST** /api/add-provider | 
[**delete_provider**](ProviderApi.md#delete_provider) | **POST** /api/delete-provider | 
[**get_global_providers**](ProviderApi.md#get_global_providers) | **GET** /api/get-global-providers | 
[**get_provider**](ProviderApi.md#get_provider) | **GET** /api/get-provider | 
[**get_providers**](ProviderApi.md#get_providers) | **GET** /api/get-providers | 
[**update_provider**](ProviderApi.md#update_provider) | **POST** /api/update-provider | 



## add_provider

> models::ControllersResponse add_provider(body)


add provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Provider**](Provider.md) | The details of the provider | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_provider

> models::ControllersResponse delete_provider(body)


delete provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Provider**](Provider.md) | The details of the provider | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_global_providers

> Vec<models::Provider> get_global_providers()


get Global providers

### Parameters

This endpoint does not need any parameter.

### Return type

[**Vec<models::Provider>**](object.Provider.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_provider

> models::Provider get_provider(id)


get provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the provider | [required] |

### Return type

[**models::Provider**](object.Provider.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_providers

> Vec<models::Provider> get_providers(owner)


get providers

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of providers | [required] |

### Return type

[**Vec<models::Provider>**](object.Provider.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_provider

> models::ControllersResponse update_provider(id, body)


update provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the provider | [required] |
**body** | [**Provider**](Provider.md) | The details of the provider | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

