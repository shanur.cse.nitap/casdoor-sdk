# Model

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_time** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**model_text** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


