# casdoor\ServiceApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**send_email**](ServiceApi.md#send_email) | **POST** /api/send-email | 
[**send_notification**](ServiceApi.md#send_notification) | **POST** /api/send-notification | 
[**send_sms**](ServiceApi.md#send_sms) | **POST** /api/send-sms | 



## send_email

> models::ControllersResponse send_email(client_id, client_secret, from)


This API is not for Casdoor frontend to call, it is for Casdoor SDKs.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**client_id** | **String** | The clientId of the application | [required] |
**client_secret** | **String** | The clientSecret of the application | [required] |
**from** | [**ControllersEmailForm**](ControllersEmailForm.md) | Details of the email request | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## send_notification

> models::ControllersResponse send_notification(from)


This API is not for Casdoor frontend to call, it is for Casdoor SDKs.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**from** | [**ControllersNotificationForm**](ControllersNotificationForm.md) | Details of the notification request | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## send_sms

> models::ControllersResponse send_sms(client_id, client_secret, from)


This API is not for Casdoor frontend to call, it is for Casdoor SDKs.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**client_id** | **String** | The clientId of the application | [required] |
**client_secret** | **String** | The clientSecret of the application | [required] |
**from** | [**ControllersSmsForm**](ControllersSmsForm.md) | Details of the sms request | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

