# casdoor\SyncerApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_syncer**](SyncerApi.md#add_syncer) | **POST** /api/add-syncer | 
[**delete_syncer**](SyncerApi.md#delete_syncer) | **POST** /api/delete-syncer | 
[**get_syncer**](SyncerApi.md#get_syncer) | **GET** /api/get-syncer | 
[**get_syncers**](SyncerApi.md#get_syncers) | **GET** /api/get-syncers | 
[**run_syncer**](SyncerApi.md#run_syncer) | **GET** /api/run-syncer | 
[**update_syncer**](SyncerApi.md#update_syncer) | **POST** /api/update-syncer | 



## add_syncer

> models::ControllersResponse add_syncer(body)


add syncer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Syncer**](Syncer.md) | The details of the syncer | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_syncer

> models::ControllersResponse delete_syncer(body)


delete syncer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Syncer**](Syncer.md) | The details of the syncer | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_syncer

> models::Syncer get_syncer(id)


get syncer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the syncer | [required] |

### Return type

[**models::Syncer**](object.Syncer.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_syncers

> Vec<models::Syncer> get_syncers(owner)


get syncers

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of syncers | [required] |

### Return type

[**Vec<models::Syncer>**](object.Syncer.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## run_syncer

> models::ControllersResponse run_syncer(body)


run syncer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Syncer**](Syncer.md) | The details of the syncer | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_syncer

> models::ControllersResponse update_syncer(id, body)


update syncer

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the syncer | [required] |
**body** | [**Syncer**](Syncer.md) | The details of the syncer | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

