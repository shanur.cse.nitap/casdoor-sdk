# casdoor\VerificationApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**send_verification_code**](VerificationApi.md#send_verification_code) | **POST** /api/send-verification-code | 
[**verify_captcha**](VerificationApi.md#verify_captcha) | **POST** /api/verify-captcha | 
[**verify_code**](VerificationApi.md#verify_code) | **POST** /api/verify-code | 



## send_verification_code

> models::Userinfo send_verification_code()


### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## verify_captcha

> models::Userinfo verify_captcha()


### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## verify_code

> models::Userinfo verify_code()


### Parameters

This endpoint does not need any parameter.

### Return type

[**models::Userinfo**](object.Userinfo.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

