# Permission

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**actions** | Option<**Vec<String>**> |  | [optional]
**adapter** | Option<**String**> |  | [optional]
**approve_time** | Option<**String**> |  | [optional]
**approver** | Option<**String**> |  | [optional]
**created_time** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**domains** | Option<**Vec<String>**> |  | [optional]
**effect** | Option<**String**> |  | [optional]
**groups** | Option<**Vec<String>**> |  | [optional]
**is_enabled** | Option<**bool**> |  | [optional]
**model** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**resource_type** | Option<**String**> |  | [optional]
**resources** | Option<**Vec<String>**> |  | [optional]
**roles** | Option<**Vec<String>**> |  | [optional]
**state** | Option<**String**> |  | [optional]
**submitter** | Option<**String**> |  | [optional]
**users** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


