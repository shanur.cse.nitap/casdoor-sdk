# casdoor\CertApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**add_cert**](CertApi.md#add_cert) | **POST** /api/add-cert | 
[**delete_cert**](CertApi.md#delete_cert) | **POST** /api/delete-cert | 
[**get_cert**](CertApi.md#get_cert) | **GET** /api/get-cert | 
[**get_certs**](CertApi.md#get_certs) | **GET** /api/get-certs | 
[**get_global_certs**](CertApi.md#get_global_certs) | **GET** /api/get-global-certs | 
[**update_cert**](CertApi.md#update_cert) | **POST** /api/update-cert | 



## add_cert

> models::ControllersResponse add_cert(body)


add cert

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Cert**](Cert.md) | The details of the cert | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## delete_cert

> models::ControllersResponse delete_cert(body)


delete cert

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**body** | [**Cert**](Cert.md) | The details of the cert | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_cert

> models::Cert get_cert(id)


get cert

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the cert | [required] |

### Return type

[**models::Cert**](object.Cert.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_certs

> Vec<models::Cert> get_certs(owner)


get certs

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**owner** | **String** | The owner of certs | [required] |

### Return type

[**Vec<models::Cert>**](object.Cert.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## get_global_certs

> Vec<models::Cert> get_global_certs()


get globle certs

### Parameters

This endpoint does not need any parameter.

### Return type

[**Vec<models::Cert>**](object.Cert.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## update_cert

> models::ControllersResponse update_cert(id, body)


update cert

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** | The id ( owner/name ) of the cert | [required] |
**body** | [**Cert**](Cert.md) | The details of the cert | [required] |

### Return type

[**models::ControllersResponse**](controllers.Response.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: */*

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

