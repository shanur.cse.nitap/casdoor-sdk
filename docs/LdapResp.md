# ControllersLdapResp

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**exist_uuids** | Option<**Vec<String>**> |  | [optional]
**users** | Option<[**Vec<models::LdapUser>**](object.LdapUser.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


